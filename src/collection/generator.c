#include <stdio.h>
#include <stdlib.h>

#include "iter.h"

struct iter
{
	it_vtable_t *vtable;
	void *(*func)(void * arg);
	void * arg;
};

static void next(iter_t *this)
{
	this->arg = this->func(this->arg);
}

static bool has_next(iter_t *this)
{
	return this->arg != NULL;
}

static void * data(iter_t *this)
{
	return this->arg;
}

static void fi_free(iter_t *this)
{
	free(this);
}

static it_vtable_t vtable = {
    &next, &has_next, &data, &fi_free};

iter_t *generator_iterator(void*(*func)(void *arg), void * arg)
{
	iter_t *this = malloc(sizeof(iter_t));
	if (this)
	{
		this->vtable = &vtable;
		this->func = func;
		this->arg  = arg;//func(arg);
	}
	return this;
}