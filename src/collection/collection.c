#include "collection.h"
#include <stdlib.h>

#include "iter.h"

void * col_add(col_t * this, const void * data, int count) 
{
    if(!this) return NULL;
    return this->vtable->add(this, data, count);
}

int col_size(col_t * this) {
    if(!this) return -1;
    return this->vtable->size(this);
}

iter_t * col_get_iterator(col_t * this) {
    if(!this) return NULL;
    return this->vtable->get_iterator(this);
}

void col_clear_all(col_t * this){
    if(!this) return;
    this->vtable->clear_all(this);
}

void col_remove_if(col_t * this, remove_if_fn remove) 
{
    if(!this) return;
    this->vtable->remove_if(this, remove);
}

void col_free(col_t * this) 
{
    if(!this) return;
    this->vtable->free(this);
}

void col_foreach(col_t *this, col_foreach_fn foreach_fn) 
{
    if(!this) return;
    iter_t * it = col_get_iterator(this);
    while(it_has_next(it)) {
        foreach_fn(it_data(it));
        it_next(it);
    }
    it_free(it);
}

void col_fold(col_t *this, col_fold_fn fold_fn, void * arg) 
{
    if(!this) return;
    iter_t * it = col_get_iterator(this);
    while(it_has_next(it)) {
        fold_fn(it_data(it), arg);
        it_next(it);
    }
    it_free(it);
}