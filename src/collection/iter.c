

#include <stdlib.h>

#include "iter.h"
#include "collection.h"

typedef struct iter {
    it_vtable_t * vtable;
} iter_t;

void it_next(iter_t * this) 
{
    if(this->vtable->next) {
        this->vtable->next(this);
    }
}

bool it_has_next(iter_t * this)
{
    if(this->vtable->has_next) {
        return this->vtable->has_next(this);
    } else {
        return false;
    }
}

void* it_data(iter_t * this)
{
    if(this->vtable->data) {
        return this->vtable->data(this);
    } else {
        return NULL;
    }
}

void it_free(iter_t * this) {
    if(this->vtable->free) {
        this->vtable->free(this);
    } else {
        free(this);
    }
}

void it_foreach(iter_t *it, col_foreach_fn foreach_fn) 
{
    if(!it) return;
    
    while(it_has_next(it)) {
        foreach_fn(it_data(it));
        it_next(it);
    }
    it_free(it);
}

void it_fold(iter_t *it, col_fold_fn fold_fn, void * arg) 
{
    if(!it) return;
    
    while(it_has_next(it)) {
        fold_fn(it_data(it), arg);
        it_next(it);
    }
    it_free(it);
}