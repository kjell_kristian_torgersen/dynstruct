#include "list.h"
#include "earray.h"

/** \brief Create an earray accessible through collection interface */
col_t * colfac_earray(int elemsize) 
{
    return (col_t *) ea_init(elemsize);
}

/** \brief Create a linked lsit accessible through collection interface */
col_t * colfac_list(void) 
{
    return (col_t *) list_init();
}