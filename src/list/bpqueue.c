#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <pthread.h>

#include "pqueue.h"
#include "bpqueue.h"
struct bpqueue {
	pqueue_t * queue;	///< Underlying pointer queue
	pthread_mutex_t mutex;	///< Mutex
	pthread_cond_t condvar;	///< Condition variable
	bool done;		///< Used for terminating all threads blocking on condvar
};
 
#define MUTEX_LOCK(x) do {  \
	pthread_mutex_lock(&this->mutex);\
	 x \
	 pthread_mutex_unlock(&this->mutex);\
	 } while(0);

bpqueue_t * bpqueue_init(void)
{
	bpqueue_t * this = malloc(sizeof(bpqueue_t));
	if(this) {
		this->queue = pqueue_init();
		pthread_mutex_init(&this->mutex, NULL);
		pthread_cond_init(&this->condvar, NULL);
		this->done = false;
	}
	return this;
}

void bpqueue_enqueue(bpqueue_t * this, void * data)
{
	pthread_mutex_lock(&this->mutex);
		pqueue_enqueue(this->queue, data);
		pthread_cond_signal(&this->condvar);
	pthread_mutex_unlock(&this->mutex);
}

void * bpqueue_dequeue(bpqueue_t * this)
{
	void * data = NULL;
	pthread_mutex_lock(&this->mutex);
	while(pqueue_size(this->queue) == 0) {
		pthread_cond_wait(&this->condvar, &this->mutex);
		if(this->done) {
			pthread_mutex_unlock(&this->mutex);
			pthread_exit(NULL);
			return NULL;
		}
	}
	data = pqueue_dequeue(this->queue);
	pthread_mutex_unlock(&this->mutex);
	return data;
}

size_t bpqueue_size(bpqueue_t * this)
{
	size_t ret;
	pthread_mutex_lock(&this->mutex);
		ret = pqueue_size(this->queue);
	pthread_mutex_unlock(&this->mutex);
	return ret;
}

void bpqueue_free(bpqueue_t * this) 
{
	pthread_mutex_destroy(&this->mutex);
	pthread_cond_destroy(&this->condvar);
	pqueue_free(this->queue);
}

void bpqueue_done(bpqueue_t * this) 
{
	this->done = true;
	pthread_cond_broadcast(&this->condvar);
}