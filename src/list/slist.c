#include <stdlib.h>
#include <string.h>

#include "collection.h"

#include "slist.h"

#include "iter.h"

typedef struct node
{
    void *data;
    struct node *next;
} node_t;

typedef struct slist
{
    col_vtable_t *vtable;   ///< vtable to conform to collection interface
    node_t *root;           ///< root of linked list
    int elemSize;           ///< element size of linked list
} slist_t;

typedef struct sl_iter
{
    it_vtable_t *vtable;
    node_t *node;
} sl_iter_t;

static void next(iter_t *this)
{
    sl_iter_t * it = (sl_iter_t *)this;
    it->node = it->node->next;
}

static bool has_next(iter_t *this)
{
    sl_iter_t * it = (sl_iter_t *)this;
    return it->node != NULL;
}

static void *data(iter_t *this)
{
    sl_iter_t * it = (sl_iter_t *)this;
    return it->node->data;
}

it_vtable_t it_vtable = {
    &next, &has_next, &data, NULL
    };

static void * add(col_t *this, const void *data, int count)
{
    (void)count;
    return sl_append((slist_t *)this, data);
}
static int size(col_t *this)
{
    return sl_size((slist_t *)this);
}
static int elemsize(col_t *this)
{
    return sl_elemsize((slist_t *)this);
}

static iter_t *get_iterator(col_t *this)
{
    slist_t * sl = (slist_t *) this;
    sl_iter_t *it = malloc(sizeof(sl_iter_t));
    if (it)
    {
        it->vtable = &it_vtable;
        it->node = sl->root;
    }
    return (iter_t *)it;
}

static void clear_all(col_t *this)
{
    sl_clear_all((slist_t *)this);
}
static void lfree(col_t *this)
{
    sl_free((slist_t *)this);
}

static int remove_if(col_t * this, remove_if_fn remove) {
    (void)this;
    (void)remove;
    return -1; // TODO: implement
}

static col_vtable_t vtable = {&add, &size, &elemsize, &get_iterator, &clear_all, &remove_if, &lfree};

slist_t *sl_init(int elemSize)
{
    slist_t *sl = malloc(sizeof(slist_t));
    sl->vtable = &vtable;
    sl->elemSize = elemSize;
    sl->root = NULL;
    return sl;
}

void * sl_append(slist_t *sl, const void *data)
{
    node_t *node = malloc(sizeof(node_t));
    node->next = sl->root;
    node->data = malloc(sl->elemSize);
    memcpy(node->data, data, sl->elemSize);
    sl->root = node;
    return node->data;
}

int sl_size(slist_t *this)
{
    if (this == NULL)
        return -1;
    int size = 0;
    for (node_t *it = this->root; it != NULL; it = it->next)
    {
        size++;
    }
    return size;
}

int sl_elemsize(slist_t *this)
{
    if (this == NULL)
        return -1;
    return this->elemSize;
}

node_t *sl_get_iterator(slist_t *sl)
{
    return sl->root;
}

node_t *sl_next(node_t *node)
{
    if (node != NULL)
    {
        return node->next;
    }
    else
    {
        return NULL;
    }
}

void *sl_get(node_t *node)
{
    if (node == NULL)
        return NULL;
    return node->data;
}

void sl_clear_all(slist_t *this)
{
    node_t *prev = NULL;
    for (node_t *it = this->root; it != NULL; it = it->next)
    {
        if (prev != NULL)
        {
            free(prev->data);
            free(prev);
        }
        prev = it;
    }
    free(prev->data);
    free(prev);
    this->root = NULL;
}

void sl_free(slist_t *sl)
{
    sl_clear_all(sl);
    free(sl);
}