#ifndef __LIST_PRIV_H__
#define __LIST_PRIV_H__

#include "collection.h"
#include "iter.h"

struct node
{
    struct node *next;  ///< next element in linked list or NULL
    char data[];        ///< access to data
};

struct list
{
    col_vtable_t * vtable;  ///< vtable since this conforms to the collection interface
    struct node * root;     ///< root of linked list
    struct node * last;     ///< last node of linked list
};

col_vtable_t * list_vtable(void);
it_vtable_t * list_it_vtable(void);

#endif