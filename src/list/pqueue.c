#include <stdlib.h>

#include "pqueue.h"

typedef struct node {
	void * data;		///< storage of pointer in pointer queue
	struct node * next;	///< next node
} node_t;

struct pqueue {
	node_t * first;		///< first node in linked list
	node_t * last;		///< last node in linked list
	size_t size;		///< size of linked list in number of nodes
};

pqueue_t * pqueue_init(void)
{
	pqueue_t * this = malloc(sizeof(pqueue_t));
	if(this) {
		this->first = NULL;
		this->last = NULL;
		this->size = 0;
	}
	return this;
}

void pqueue_enqueue(pqueue_t * this, void * data)
{
	node_t * newNode = malloc(sizeof(node_t));
	newNode->data = data;
	newNode->next = NULL;

	if(this->last == NULL) {
		this->last = this->first = newNode;
	} else {
		this->last->next = newNode;
        	this->last = newNode;
	}
	this->size++;
}

void * pqueue_dequeue(pqueue_t * this)
{
	if(this->first == NULL) {
		return NULL;
	} else {
		node_t * tmp;
		void * data = this->first->data;
		tmp = this->first;
		this->first = this->first->next;
		this->size--;
		if(this->size == 0) {
			this->first = this->last = NULL;
		}
		free(tmp);
		return data;
	}
}

size_t pqueue_size(pqueue_t * this)
{
	return this->size;
}

void pqueue_free(pqueue_t * this) 
{
	if(!this) return;
	node_t * it_last = NULL;
	for(node_t * it = this->first; it != NULL; it = it->next) {
		if(it_last) {
			free(it_last);
		}
		it_last = it;
	}
	if(it_last) {
		free(it_last);
	}
	free(this);
}