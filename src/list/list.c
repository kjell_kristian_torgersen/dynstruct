#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "collection.h"
#include "iter.h"

#include "list.h"
#include "list_priv.h"

typedef struct list_iter list_iter_t;

list_t *list_init(void)
{
    list_t *ret = malloc(sizeof(list_t));
    if (ret)
    {
        ret->vtable = list_vtable();
        ret->root = NULL;
    }
    return ret;
}

static node_t *node_create(node_t *next, const void *data, int count)
{
    node_t *ret = malloc(sizeof(node_t) + count);
    if (ret)
    {
        ret->next = next;
        memcpy(ret->data, data, count);
    }
    return ret;
}

void * list_add(list_t *this, const void *data, int count)
{
    if (!this || !data)
    {
        return NULL;
    }
    if(this->root == NULL) {
        this->root = this->last = node_create(NULL, data, count);
        return this->root->data;
    } else {
        this->last->next = node_create(NULL, data, count);
        this->last = this->last->next;
        return this->last->data;
    }
    //this->root = node_create(this->root, data, count);
}

void list_remove(list_t *this, node_t *remove)
{
    if (!this || !remove)
    {
        return;
    }
    if (remove == this->root)
    {
        this->root = this->root->next;
        free(remove);
    }
    else
    {
        node_t *prev = NULL;
        for (node_t *it = lit_get(this); it != NULL; it = lit_next(it))
        {
            if (it == remove)
            {
                if(it == this->last) {
                    this->last = prev;
                } else {
                    prev->next = it->next;
                }
                free(remove);
                return;
            }
            prev = it;
        }
    }
}

int list_remove_if(list_t *this, remove_if_fn remove)
{
    if (!this || !remove)
    {
        return 0;
    }
    if (this->root == NULL)
    {
        return 0;
    }
    int removed = 0;
    while (remove(this->root->data))
    {
        // remove root node
        node_t *oldroot = this->root;
        this->root = oldroot->next;
        free(oldroot);
        removed++;
        if(this->root == NULL) return removed ;
    }

    for (node_t *it = this->root; it != NULL; it = it->next)
    {
        if (it->next)
        {
            if (remove(it->next->data))
            {
                node_t *oldnode = it->next;
                it->next = it->next->next;
                free(oldnode);
                removed++;
            }
        }
    }

    // update last node
    for (node_t *it = this->root; it != NULL; it = it->next)
    {
        if (it->next == NULL)
        {
            this->last = it;
        }
    }

    return removed;
}

void list_foreach(list_t *this, col_foreach_fn foreach_fn)
{
    if(this && foreach_fn) {
        for (node_t *it = this->root; it != NULL; it = it->next)
        {
            foreach_fn(it->data);
        }
    }
}

void list_fold(list_t *this, col_fold_fn fold_fn, void * arg)
{
    for (node_t *it = this->root; it != NULL; it = it->next)
    {
        fold_fn(it->data, arg);
    }
}

void list_remove_all(list_t *this)
{
    if (!this)
    {
        return;
    }
    node_t *prev = NULL;
    for (node_t *it = lit_get(this); it != NULL; it = lit_next(it))
    {
        if (prev != NULL)
        {
            free(prev);
        }
        prev = it;
    }
    free(prev);
    this->root = NULL;
    this->last = NULL;
}

void list_free(list_t *this)
{
    if(this) {
        list_remove_all(this);
        free(this);
    }
}

int list_size(const list_t *this)
{
    int count = 0;
    for (node_t *it = lit_get(this); it != NULL; it = lit_next(it))
    {
        count++;
    }
    return count;
}

node_t *lit_get(const list_t *this)
{
    if (!this)
    {
        return NULL;
    }
    return this->root;
}

node_t *lit_next(node_t *this)
{
    if (!this)
    {
        return NULL;
    }
    return this->next;
}

void *lit_data(node_t *this)
{
    if (!this)
    {
        return NULL;
    }
    return this->data;
}
