#include <stdlib.h>

#include "list.h"
#include "list_priv.h"

#include "iter.h"

static int size(col_t * this);
static int elemsize(col_t * this);
static iter_t * get_iterator(col_t * this);
static void clear_all(col_t * this);
static int remove_if(col_t * this, remove_if_fn remove);
static void next(iter_t * this);
static bool has_next(iter_t * this);
static void* data(iter_t * this);

typedef struct list_iter {
    it_vtable_t * vtable;
    node_t * node;
} list_iter_t;

static it_vtable_t it_vtable = {
&next, &has_next, &data, NULL
};

static void lfree(col_t * this) {
    list_free((list_t * )this);
}

static void * ladd(col_t * this, const void * data, int count) 
{
    return list_add((list_t*)this, data, count);
}

static col_vtable_t vtable = {
    &ladd, &size, &elemsize, &get_iterator, &clear_all, &remove_if, &lfree
};

static int size(col_t * this) 
{
    return list_size((list_t*)this);
}

static int elemsize(col_t * this) 
{
    (void)this;
    return -1;
    //return list_size((list_t*)this);
}

static iter_t * get_iterator(col_t * this)
{
    list_t * list = (list_t*)this;
    list_iter_t * it = malloc(sizeof(list_iter_t));
    if(it) {
        it->vtable = &it_vtable;
        it->node = list->root;
    }
    return (iter_t *)it;
}

static void clear_all(col_t * this)
{
    list_remove_all((list_t*)this);
}

static int remove_if(col_t * this, remove_if_fn remove)
{
    return list_remove_if((list_t*)this, remove);
}

static void next(iter_t * this) {
    list_iter_t * it = (list_iter_t *)this;
    it->node = it->node->next;
}
static bool has_next(iter_t * this) {
    list_iter_t * it = (list_iter_t *)this;
    return it->node;
}
static void* data(iter_t * this) {
    list_iter_t * it = (list_iter_t *)this;
    return it->node->data;
}

col_vtable_t * list_vtable(void) 
{
    return &vtable;
}

it_vtable_t * list_it_vtable(void) 
{
    return &it_vtable;
}