#include <stdlib.h>
#include <string.h>

#include "dqueue.h"

#define MIN(A,B) (((A)<(B))?(A):(B))

typedef struct node {
	struct node * next;	///< Next node in queue
	size_t count;		///< Number of bytes stored at data[]
	char data[];		///< Access to data stored in node
} node_t;

struct dqueue {
	node_t * first;		///< First node in linked list
	node_t * last;		///< Last node in linked list
	size_t size;		///< Size of linked list in elements
};

dqueue_t * dqueue_init(void)
{
	dqueue_t * this = malloc(sizeof(dqueue_t));
	if(this) {
		this->first = NULL;
		this->last = NULL;
		this->size = 0;
	}
	return this;
}

void dqueue_enqueue(dqueue_t * this, const void * data, size_t count)
{
	node_t * newNode = malloc(sizeof(node_t) + count);
	newNode->count = count;
	memcpy(newNode->data, data, count);
	newNode->next = NULL;

	if(this->last == NULL) {
		this->last = this->first = newNode;
	} else {
		this->last->next = newNode;
        	this->last = newNode;
	}
	this->size++;
}

size_t dqueue_dequeue(dqueue_t * this, void * buf, size_t count)
{
	if(this->first == NULL) {
		return 0;
	} else {
		node_t * tmp;
		void * data = this->first->data;
		size_t n = MIN(count, this->first->count);
		memcpy(buf, data, n);
		tmp = this->first;
		this->first = this->first->next;
		this->size--;
		if(this->size == 0) {
			this->first = this->last = NULL;
		}
		free(tmp);
		return n;
	}
}

size_t dqueue_size(dqueue_t * this)
{
	return this->size;
}

void dqueue_free(dqueue_t * this) 
{
	if(!this) return;
	node_t * it_last = NULL;
	for(node_t * it = this->first; it != NULL; it = it->next) {
		if(it_last) {
			free(it_last);
		}
		it_last = it;
	}
	if(it_last) {
		free(it_last);
	}
	free(this);
}