#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

#include "dqueue.h"
#include "bdqueue.h"
struct bdqueue {
	dqueue_t * queue;	///< Underlying data queue
	pthread_mutex_t mutex;	///< Mutex used together with condition variable condvar
	pthread_cond_t condvar;	///< Condition variable used to sleep threads waiting for data
	bool done;		///< Help terminating threads waiting for data when done
};
 
#define MUTEX_LOCK(x) do {  \
	pthread_mutex_lock(&this->mutex);\
	 x \
	 pthread_mutex_unlock(&this->mutex);\
	 } while(0);

bdqueue_t * bdqueue_init(void)
{
	bdqueue_t * this = malloc(sizeof(bdqueue_t));
	if(this) {
		this->queue = dqueue_init();
		pthread_mutex_init(&this->mutex, NULL);
		pthread_cond_init(&this->condvar, NULL);
		this->done = false;
	}
	return this;
}

void bdqueue_enqueue(bdqueue_t * this, const void * data, size_t count)
{
	pthread_mutex_lock(&this->mutex);
		dqueue_enqueue(this->queue, data, count);
		pthread_cond_signal(&this->condvar);
	pthread_mutex_unlock(&this->mutex);
}

size_t bdqueue_dequeue(bdqueue_t * this, void * buf, size_t count)
{
	size_t ret = 0;
	pthread_mutex_lock(&this->mutex);
	while(dqueue_size(this->queue) == 0) {
		pthread_cond_wait(&this->condvar, &this->mutex);
		if(this->done) {
			pthread_mutex_unlock(&this->mutex);
			pthread_exit(NULL);
			return 0;
		}
	}
	ret = dqueue_dequeue(this->queue, buf, count);
	pthread_mutex_unlock(&this->mutex);
	return ret;
}

size_t bdqueue_size(bdqueue_t * this)
{
	size_t ret;
	pthread_mutex_lock(&this->mutex);
		ret = dqueue_size(this->queue);
	pthread_mutex_unlock(&this->mutex);
	return ret;
}

void bdqueue_free(bdqueue_t * this) 
{
	pthread_mutex_destroy(&this->mutex);
	pthread_cond_destroy(&this->condvar);
	dqueue_free(this->queue);
}

void bdqueue_done(bdqueue_t * this) 
{
	this->done = true;
	pthread_cond_broadcast(&this->condvar);
}