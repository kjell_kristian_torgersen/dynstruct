#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char *strmerge(const char *str, ...)
{
    va_list valist;
    int sum = 0;
    int offset = 0;
    char * ret = NULL;
    const char * arg;

    sum += strlen(str);
    va_start(valist, str);
    while ((arg = va_arg(valist, const char*)) != NULL)
    {
        sum += strlen(arg);
    }
    va_end(valist);

    ret = malloc(sum + 1);

    strcpy(&ret[offset], str);
    offset += strlen(str);
    va_start(valist, str);
    while ((arg = va_arg(valist, const char*)) != NULL)
    {
        strcpy(&ret[offset], arg);
        offset += strlen(arg);
    }
    ret[offset] = '\0';
    va_end(valist);

    return ret;
}

void * memdup(void * src, int count) 
{
        void * ret = malloc(count);
        if(ret) {
                memcpy(ret, src, count);
        }
        return ret;
}

char * substr(char * str, int count) 
{
        char * ret = malloc(count + 1);
        if(ret) {
                memcpy(ret, str, count);
                ret[count] = '\0';
        }
        return ret;
}

bool strfind(char * str, char * what) 
{
        return !strncmp(str, what, strlen(what));
}