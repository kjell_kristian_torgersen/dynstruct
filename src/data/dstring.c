#include "collection.h"
#include "col_factory.h"

col_t *string_split(const char *str, char sep)
{
        col_t *ret = colfac_list();
        if (ret)
        {
                int i = 0;
                int lastsep = 0;
                while (str[i] != '\0')
                {
                        if (str[i] == sep)
                        {
                                int count = i - lastsep;
                                char *copy = col_add(ret, &str[lastsep], count);
                                copy[count] = '\0';
                                lastsep = i + 1;
                        }
                        i++;
                }
                int count = i - lastsep;
                char *copy = col_add(ret, &str[lastsep], count);
                copy[count] = '\0';
                lastsep = i + 1;
        }
        return ret;
}
