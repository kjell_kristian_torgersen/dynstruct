#include <stdlib.h>

#include "column.h"
#include "misc.h"

void column_fromstring(column_t * this, char *str) 
{
      int ustart = 0;
      int ustop = 0;
      int i = 0;
      while(str[i] != '\0') {
              if(str[i] == '[') {
                      ustart = i;
              }
              if(str[i] == ']') {
                      ustop = i;
              }
              i++;
      }
      this->name = substr(str, ustart - 1);
      this->unit = substr(&str[ustart + 1], ustop-ustart-1);
}

void column_free(column_t * this) 
{
        free(this->name);
        free(this->unit);
}