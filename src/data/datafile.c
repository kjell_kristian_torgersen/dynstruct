#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "datafile.h"

#include "col_factory.h"
#include "datetime.h"
#include "misc.h"

struct datafile {
        char * description;
        datetime_t datetime;
        int columns;
        column_t * cols;
        col_t * rows;
};

datafile_t * datafile_init(char * description, datetime_t * datetime, int columns, column_t * cols) 
{
        datafile_t * this = malloc(sizeof(datafile_t));
        if(this) {
                this->description = strdup(description);
                this->datetime = *datetime;
                this->columns = columns;
                this->cols = memdup(cols, columns*sizeof(column_t));
                this->rows = colfac_earray(sizeof(double)*columns);
        }
        return this;
}

char * datafile_description(datafile_t * this) 
{
        return this->description;
}

datetime_t * datafile_datetime(datafile_t * this)
{
        return &this->datetime;
}

void datafile_addrow(datafile_t * this, double * row) 
{
        col_add(this->rows, row, sizeof(double)*this->columns);
}

col_t * datafile_getrows(datafile_t * this) 
{
        return this->rows;
}

column_t * datafile_getcols(datafile_t * this) 
{
        return this->cols;
}

int datafile_getcolumns(datafile_t * this) 
{
        return this->columns;
}

void datafile_free(datafile_t * this) 
{
        free(this->description);
        free(this->cols);
        col_free(this->rows);
        free(this);
}