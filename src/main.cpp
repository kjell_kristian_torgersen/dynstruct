#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "collection.h"
#include "darray.h"
#include "directory.h"
#include "earray.h"
#include "file.h"
#include "iter.h"
#include "list.h"
#include "log.h"
#include "misc.h"
#include "slist.h"
#include "x11gfx.h"

void putint(int *i)
{
	printf("%i ", *i);
}

void addone(int *dst, int *src)
{
	*dst = *src + 1;
}

void mul2(int *dst, int *src)
{
	*dst = (*src) * 2;
}

void print_int_array(earray_t *ea)
{
	ea_foreach(ea, (ea_foreach_fn_t)putint);
	printf("\n");
}

typedef struct minmax
{
	int min;
	int max;
	int sum;
	int count;
} minmax_t;

void minmax_f(int *elem, minmax_t *minmax)
{
	if (*elem > minmax->max)
		minmax->max = *elem;
	if (*elem < minmax->min)
		minmax->min = *elem;
	minmax->sum += *elem;
	minmax->count++;
}

int intcmp(const int *a, const int *b)
{
	return *a - *b;
}

void test_earray(void)
{
	earray_t *ea = ea_init(sizeof(int));

	for (int j = 0; j < 10; j++)
	{
		int u = (rand() >> 2) % 10;
		ea_append(ea, &u);
	}
	printf("Original: ");
	print_int_array(ea);
	ea_sort(ea, (ea_compare_fn_t)intcmp);
	printf("Sorted: ");
	print_int_array(ea);
	earray_t *result = ea_map(ea, sizeof(int), (ea_map_fn_t)mul2);
	print_int_array(result);

	minmax_t minmax;
	minmax.count = 0;
	minmax.max = INT_MIN;
	minmax.min = INT_MAX;
	minmax.sum = 0;

	ea_fold(result, (ea_fold_fn_t)minmax_f, &minmax);

	printf("%i %i %i %i\n", minmax.max, minmax.min, minmax.count, minmax.sum);

	ea_free(result);
	ea_free(ea);
}

void test_slist(void)
{
	slist_t *sl = sl_init(sizeof(int));
	for (int i = 0; i < 10; i++)
	{
		sl_append(sl, &i);
	}

	for (node_t *it = sl_get_iterator(sl); it != NULL; it = sl_next(it))
	{
		printf("%i ", *(int *)sl_get(it));
	}
	printf("\n");

	sl_free(sl);
}

void print_int_list(list_t *list)
{
	printf("count = %i\n", list_size(list));
	for (node_t *it = lit_get(list); it != NULL; it = lit_next(it))
	{
		printf("%i ", *(int *)lit_data(it));
	}
	printf("\n");
}

void test_list()
{
	list_t *list = list_init();

	for (int j = 0; j < 10; j++)
	{
		list_remove_all(list);
		for (int i = 0; i < 10; i++)
		{
			list_add(list, &i, sizeof(i));
		}

		//print_int_list(list);
		node_t *remove = lit_get(list);
		for (int k = 0; k < j; k++)
		{
			remove = lit_next(remove);
		}
		list_remove(list, remove);

		print_int_list(list);
	}

	list_free(list);
}

list_t *file_readAllLines(char *path)
{
	//	list_t * l = list_init();
	//char buf[4096];
	return NULL;
}

void test_darray()
{
	darray_t *arr = da_init();
	da_add(arr, "Hello ", 6);
	da_add(arr, "world!", 7);

	da_remove(arr, 100, 1);
	da_add(arr, "\0", 1);
	printf("%s\n", (char *)da_at(arr, 1));

	da_free(arr);
}

void test_x11(void);

void print_ints(col_t *col)
{
	iter_t *it = col_get_iterator(col);
	while (it_has_next(it))
	{
		printf("%i\n", *(int *)it_data(it));
		it_next(it);
	}
	it_free(it);

	printf("size=%i\n", col_size(col));
}

void print_ints2(col_t *col)
{
	col_foreach(col, (col_foreach_fn)putint);
	printf("size=%i\n", col_size(col));
}

void test_iterators()
{
	col_t *list = (col_t *)list_init();
	col_t *ea = (col_t *)ea_init(sizeof(int));

	for (int i = 0; i < 10; i++)
	{
		col_add(list, &i, sizeof(i));
		col_add(ea, &i, sizeof(i));
	}

	print_ints2(list);
	print_ints2(ea);

	col_free(list);
	col_free(ea);
}

bool isOdd(int *data)
{
	return (*data) & 1;
}

bool isEven(int *data)
{
	return !((*data) & 1);
}

void test_remove_if(void)
{
	//col_t * col = (col_t*) ea_init(sizeof(int));
	col_t *col = (col_t *)list_init();
	for (int i = 0; i < 10; i++)
	{
		col_add(col, &i, sizeof(i));
	}

	minmax_t minmax;
	minmax.count = 0;
	minmax.max = 0;
	minmax.min = 100;
	minmax.sum = 0;

	col_fold(col, (col_fold_fn)&minmax_f, &minmax);

	printf("%i %i %i %i\n", minmax.count, minmax.max, minmax.min, minmax.sum);

	printf("before remove: ");
	print_ints2(col);
	col_remove_if(col, (remove_if_fn)isEven);
	printf("after remove: ");
	print_ints2(col);
	col_free(col);
}

void print_file_info(void *info)
{
	struct entry_info *file_info = info;
	printf("%s\t%s\n", dir_type(file_info) == FT_FILE ? "FILE" : "DIR ", dir_name(file_info));
}

void test_dir_get_files(char *path);

void process_file_dir(void *elem, void *arg)
{
	char *path = (char *)arg;
	struct entry_info *info = (struct entry_info *)elem;

	char *recpath = strmerge(path, "/", dir_name(info), NULL);
	if (dir_type(info) == FT_DIR)
	{
		test_dir_get_files(recpath);
	}
	else
	{
		printf("%s %li\n", recpath, dir_size(recpath));
	}
	free(recpath);
}

void test_dir_get_files(char *path)
{
	col_t *files = dir_get_files(path);
	col_fold(files, &process_file_dir, path);
	col_free(files);
}

void test_file(void)
{
	//array_t * arr = file_read_all_bytes("/home/kjell/vscode.tar.gz");
	//file_write_all_bytes("/home/kjell/vscode2.tar.gz", arr);
	//array_free(arr);
}

iter_t *file_iter_lines(const char *path);

int main(int argc, char *argv[])
{
	/*col_t * lines = file_read_all_lines("/home/kjell/dataclass.txt");
	col_foreach(lines, (col_foreach_fn_t)puts);
	col_free(lines);*/

	it_foreach(file_iter_lines("/home/kjell/dataclass.txt"), (col_foreach_fn)puts);

	/*log_open("program.log");
	log_info("test of info");
	log_warning("test of warning");
	log_error("test of error");
	log_close();*/
	//test_file();
	//test_iterators();
	//test_x11();
	//	test_remove_if();
	//if (argc > 1)
	//	{
	//	test_dir_get_files(argv[1]);
	//	}
	//char * merge = strmerge("Hello", " ", "World", NULL);
	//printf("%s\n", merge);
	//free(merge);
	return 0;
}
