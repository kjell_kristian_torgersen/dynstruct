#include <stddef.h>

static size_t partition(void ** A, size_t lo, size_t hi, int(*compare_fn)(const void*,const void*)) 
{
    void * pivot = A[(hi + lo) / 2];
    size_t i = lo;
    size_t j = hi;
    for(;;) {
        while (compare_fn(A[i], pivot) < 0) { i++; }
        while (compare_fn(A[j], pivot) > 0) { j--; }
        if (i >= j) {
            return j;
        }
        // swap A[i] with A[j]
        void * tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
        i++;
        j--;
    }
}

void parray_quicksort_priv(void ** A, size_t lo, size_t hi, int(*compare_fn)(const void*,const void*)) 
{
    if (lo < hi){
        size_t partition_border = partition(A, lo, hi,compare_fn);
        parray_quicksort_priv(A, lo, partition_border,compare_fn);
        parray_quicksort_priv(A, partition_border + 1, hi,compare_fn);
    }
}

