#include <stdlib.h>

#include "iter.h"

struct iter {
    it_vtable_t * vtable;
    void ** current;
    void ** last;
};

void i_next(iter_t * this)
{
	this->current++;
}

bool i_has_next(iter_t * this)
{
	return this->current != this->last;
}

void* i_data(iter_t * this)
{
	return *this->current;
}

void i_free(iter_t * this)
{
	free(this);
}

static it_vtable_t iter_vtable = {i_next, i_has_next, i_data, i_free};

iter_t * parray_iter_init(void ** first, void ** last) 
{
	iter_t * this = malloc(sizeof(iter_t));
	if(this) {
		this->vtable = &iter_vtable;
		this->current = first;
		this->last = last;
	}
	return this;
}
