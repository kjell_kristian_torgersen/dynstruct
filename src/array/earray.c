#include "earray.h"
#include "earray_priv.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <string.h>
#include "collection.h"
//#include "iter.h"
#define MAGIC 0xa75f45b4a8bc1794

static earray_t *earray_init(int elemsize, int size, int capacity)
{
    earray_t *ea = (earray_t *)malloc(sizeof(earray_t));
    if (ea)
    {
        ea->vtable = ea_vtable();
        ea->elemsize = elemsize;
        ea->size = size;
        ea->capacity = capacity;
        ea->data = malloc(ea->elemsize * ea->capacity);
    }
    return ea;
}

earray_t *ea_init(int elemsize)
{
    return earray_init(elemsize, 0, 1);
}

static void ea_ensureCapacity(earray_t *ea, int n)
{
    if (ea == NULL)
        return;
    if (ea->size + n >= ea->capacity)
    {
        int newCapacity = (ea->size + n) * 2;
        //printf("increasing capacity from %i to %i\n", ea->capacity, newCapacity);
        void *newData = malloc(ea->elemsize * newCapacity);
        memcpy(newData, ea->data, ea->size * ea->elemsize);
        free(ea->data);
        ea->data = newData;
        ea->capacity = newCapacity;
    }
}

void * ea_append(earray_t *ea, const void *data)
{
    if (ea == NULL)
        return NULL;
    ea_ensureCapacity(ea, 1);
    memcpy(ea->data + ea->size * ea->elemsize, data, ea->elemsize);
    ea->size++;
    return ea->data + ea->size * ea->elemsize;
}

void ea_appendn(earray_t *ea, const void *data, int count)
{
    if (ea == NULL)
        return;
    ea_ensureCapacity(ea, count);
    memcpy(ea->data + ea->size * ea->elemsize, data, ea->elemsize * count);
    ea->size += count;
}

void ea_insert(earray_t *ea, int idx, const void *data)
{
    if (ea == NULL)
        return;
    if (idx >= 0 && idx <= ea->size)
    {
        ea_ensureCapacity(ea, 1);
        void *src = ea->data + idx * ea->elemsize;
        void *dest = ea->data + (idx + 1) * ea->elemsize;
        int n = (ea->size - idx) * ea->elemsize;
        memmove(dest, src, n);
        memcpy(src, data, ea->elemsize);
        ea->size++;
    }
}

void ea_insertn(earray_t *ea, int idx, const void *data, int count)
{
    if (ea == NULL)
        return;
    if (idx >= 0 && idx <= ea->size)
    {
        ea_ensureCapacity(ea, count);
        void *src = ea->data + idx * ea->elemsize;
        void *dest = ea->data + (idx + count) * ea->elemsize;
        int n = (ea->size - idx) * ea->elemsize;
        memmove(dest, src, n);
        memcpy(src, data, ea->elemsize * count);
        ea->size += count;
    }
}

void ea_write(earray_t *ea, int idx, const void *data)
{
    if (ea == NULL)
        return;
    if (idx >= 0 && idx < ea->size)
    {
        memcpy(ea->data + ea->elemsize * idx, data, ea->elemsize);
    }
}

void ea_writen(earray_t *ea, int idx, const void *data, int count)
{
    if (ea == NULL)
        return;
    if (idx >= 0 && idx + count <= ea->size)
    {
        memcpy(ea->data + ea->elemsize * idx, data, ea->elemsize * count);
    }
}

void ea_sort(earray_t *ea, ea_compare_fn_t compar)
{
    if (ea == NULL)
        return;
    qsort(ea->data, ea->size, ea->elemsize, compar);
}

earray_t *ea_map(earray_t *ea, int newElemSize, ea_map_fn_t map_fn)
{
    earray_t *ret = earray_init(newElemSize, ea->size, ea->size + 1);

    const int size = ea_size(ea);
    for (int i = 0; i < size; i++)
    {
        map_fn(ea_at(ret, i), ea_at(ea, i));
    }

    return ret;
}

void ea_fold(earray_t *ea, ea_fold_fn_t fold_fn, void *arg)
{
    const int size = ea_size(ea);
    for (int i = 0; i < size; i++)
    {
        fold_fn(ea_at(ea, i), arg);
    }
}

void ea_foreach(earray_t *ea, ea_foreach_fn_t foreach_fn)
{
    const int size = ea_size(ea);
    for (int i = 0; i < size; i++)
    {
        foreach_fn(ea_at(ea, i));
    }
}

int ea_size(earray_t *ea)
{
    if (ea == NULL)
        return -1;
    return ea->size;
}

int ea_elemsize(earray_t *ea)
{
    if (ea == NULL)
        return -1;
    return ea->elemsize;
}

void ea_set_capacity(earray_t *ea, int capacity)
{
    if (ea == NULL)
        return;
    if (capacity == ea->capacity)
        return;
    void *newData = malloc(capacity * ea->elemsize);
    int min = ea->capacity < capacity ? ea->capacity : capacity;
    memcpy(newData, ea->data, min * ea->elemsize);
    free(ea->data);
    ea->data = newData;
    ea->capacity = capacity;
    if (ea->size >= ea->capacity)
        ea->size = ea->capacity - 1;
}

int ea_capacity(earray_t *ea)
{
    if (ea == NULL)
        return -1;
    return ea->capacity;
}

void ea_erase(earray_t *ea, int idx)
{
    if (ea == NULL)
        return;
    if (idx >= 0 && idx < ea->size)
    {
        void *src = ea->data + ea->elemsize * (idx + 1);
        void *dest = ea->data + ea->elemsize * idx;
        int n = (ea->size - idx - 1) * ea->elemsize;
        memmove(dest, src, n);
        ea->size--;
    }
}

int ea_remove_if(earray_t * this, remove_if_fn remove) 
{
    int removed = 0;
    for(int i = 0; i < ea_size(this); i++) {
        if(remove(ea_at(this, i))) {
            ea_erase(this, i);
            i--;
            removed++;
        }
    }
    return removed;
}

void ea_eraseall(earray_t *ea)
{
    if (ea == NULL)
        return;
    free(ea->data);
    ea->data = malloc(ea->elemsize);
    ea->size = 0;
    ea->capacity = 1;
}

void *ea_at(earray_t *ea, int idx)
{
    if (ea == NULL)
        return NULL;
    if ((idx >= 0) && (idx < ea->size))
    {
        return ea->data + ea->elemsize * idx;
    }
    else
    {
        fprintf(stderr, "Array index %i is out of bounds for earray. Valid range is 0-%i\n", idx, ea->size - 1);
        return NULL;
    }
}

void *ea_data(earray_t *ea)
{
    if (ea == NULL)
        return NULL;
    return ea->data;
}

void ea_free(earray_t *ea)
{
    if (ea)
    {
        if (ea->data)
        {
            free(ea->data);
        }
        free(ea);
    }
}

void ea_push(earray_t * ea, void * data)
{
    ea_append(ea, data);
}

void ea_pop(earray_t * ea)
{
    if(!ea)return;
    if(ea->size) {
        ea->size--;
    }
}

void * ea_top(earray_t * ea)
{
    return ea->data + (ea->size - 1) * ea->elemsize;
}

void earray_toFile2(earray_t * this, FILE * f) 
{
    uint64_t version = 1;
    uint64_t magic = MAGIC;
    uint64_t elemsize = this->elemsize;
    uint64_t size = this->size;
    fwrite(&version, sizeof(uint64_t), 1, f);
    fwrite(&magic, sizeof(uint64_t), 1, f);
    fwrite(&elemsize, sizeof(uint64_t), 1, f);
    fwrite(&size, sizeof(uint64_t), 1, f);
    fwrite(this->data, this->elemsize, this->size, f);
}

void earray_toFile(earray_t * this, const char * path) 
{
	FILE * f = fopen(path, "wb");
	if(f) {
        earray_toFile2(this, f);
		fclose(f);
	}
}

earray_t * earray_fromFile2(FILE * f) 
{
    uint64_t version;
    uint64_t magic;
    uint64_t elemsize;
    uint64_t size;
    fread(&version, sizeof(uint64_t), 1, f);
    fread(&magic, sizeof(uint64_t), 1, f);
    if(magic != MAGIC) {
        fprintf(stderr, "earray_fromFile2: wrong magic number: %lx != %lx!\n", magic, MAGIC);
        return NULL;
    }
    fread(&elemsize, sizeof(uint64_t), 1, f);
    fread(&size, sizeof(uint64_t), 1, f);

    earray_t * ret = earray_init(elemsize, size, size);
    fread(ret->data, ret->elemsize, ret->size, f);
    return ret;
}

earray_t * earray_fromFile(const char * path) 
{
	FILE * f = fopen(path, "rb");
    earray_t * ret = NULL;
	if(f) {
        ret = earray_fromFile2(f);
        fclose(f);
	}
    return ret;
}