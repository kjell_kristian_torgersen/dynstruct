#include <stdlib.h>
#include <string.h>

#include "darray.h"

typedef struct darray
{
    int size;
    int capacity;
    char *data;
} darray_t;

static darray_t *darray_create(int size, int capacity)
{
    darray_t *ret = malloc(sizeof(darray_t));
    if (ret)
    {
        ret->size = size;
        ret->capacity = capacity;
        ret->data = malloc(ret->capacity);
    }
    return ret;
}

darray_t *da_init(void)
{
    return darray_create(0, 16);
}

darray_t *da_init_alloc(int size)
{
    return darray_create(size, size);
}

static void ensure_capacity(darray_t *this, int newSize)
{
    if (!this)
    {
        return;
    }
    if (newSize > this->capacity)
    {
        int newCapacity = 2 * newSize;
        void *newData = malloc(newCapacity);
        memcpy(newData, this->data, this->size);
        free(this->data);
        this->data = newData;
        this->capacity = newCapacity;
    }
}

void da_add(darray_t *this, const void *data, int count)
{
    if (!this || !data)
    {
        return;
    }
    ensure_capacity(this, this->size + count);
    memcpy((char*)this->data + this->size, data, count);
    this->size += count;
}

void da_insert(darray_t *this, int pos, const void *data, int count)
{
    if (!this || !data)
    {
        return;
    }
    ensure_capacity(this, this->size + count);
    memmove((char*)this->data + pos + count, this->data + pos, count);
    memcpy((char*)this->data + pos, data, count);
    this->size += count;
}
void da_remove(darray_t *this, int pos, int count)
{
    if (!this)
    {
        return;
    }
    if((pos < 0) || (pos >= this->size)) {
        return;
    }

    if(pos + count > this->size) {
        //return;
        //count = this->size - pos;
        this->size = pos;
        return;
    }

    memcpy(this->data + pos, this->data + pos + count, this->size - pos);
    this->size -= count;
}

void * da_at(darray_t * this, int pos) 
{
    if(pos < 0 || pos >= this->size) {return NULL;}
    else {
        return this->data + pos;
    }
    
}

void * da_data(darray_t * this) 
{
    return this->data;
}

void da_remove_all(darray_t * this) 
{
    this->size = 0;
}

void da_free(darray_t *this)
{
    if (!this)
    {
        return;
    }
    if (this->data)
    {
        free(this->data);
    }
    free(this);
}

int da_size(darray_t * this) 
{
    return this->size;
}

char * da_toString(darray_t * this) 
{
    char * ret = malloc(this->size + 1);
    memcpy(ret, this->data, this->size);
    ret[this->size] = '\0';
    return ret;
}