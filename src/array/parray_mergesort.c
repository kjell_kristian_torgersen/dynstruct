#include <stdlib.h>

static void CopyArray(void ** A, size_t iBegin, size_t iEnd, void ** B);
static void TopDownMerge(void ** A, size_t iBegin, size_t iMiddle, size_t iEnd, void ** B, int (*compare_fn)(const void*,const void*));
static void TopDownSplitMerge(void ** B, size_t iBegin, size_t iEnd, void ** A,int (*compare_fn)(const void*,const void*));

// Array A[] has the items to sort; array B[] is a work array.
void parray_mergesort_priv(void ** A, void ** B, size_t n,int (*compare_fn)(const void*,const void*))
{
    CopyArray(A, 0, n, B);           // one time copy of A[] to B[]
    TopDownSplitMerge(B, 0, n, A, compare_fn);   // sort data from B[] into A[]
}

// Sort the given run of array A[] using array B[] as a source.
// iBegin is inclusive; iEnd is exclusive (A[iEnd] is not in the set).
static void TopDownSplitMerge(void ** B, size_t iBegin, size_t iEnd, void ** A, int (*compare_fn)(const void*,const void*))
{
    if(iEnd - iBegin < 2)                       // if run size == 1
        return;                                 //   consider it sorted
    // split the run longer than 1 item into halves
    size_t iMiddle = (iEnd + iBegin) / 2;              // iMiddle = mid point
    // recursively sort both runs from array A[] into B[]
    TopDownSplitMerge(A, iBegin,  iMiddle, B, compare_fn);  // sort the left  run
    TopDownSplitMerge(A, iMiddle,    iEnd, B, compare_fn);  // sort the right run
    // merge the resulting runs from array B[] into A[]
    TopDownMerge(B, iBegin, iMiddle, iEnd, A, compare_fn);
}

//  Left source half is A[ iBegin:iMiddle-1].
// Right source half is A[iMiddle:iEnd-1   ].
// Result is            B[ iBegin:iEnd-1   ].
static void TopDownMerge(void ** A, size_t iBegin, size_t iMiddle, size_t iEnd, void ** B, int (*compare_fn)(const void*,const void*))
{
    size_t i = iBegin, j = iMiddle;
 
    // While there are elements in the left or right runs...
    for (size_t k = iBegin; k < iEnd; k++) {
        // If left run head exists and is <= existing right run head.
        if (i < iMiddle && (j >= iEnd || (compare_fn(A[i], A[j]) <= 0))) {
            B[k] = A[i];
            i = i + 1;
        } else {
            B[k] = A[j];
            j = j + 1;
        }
    }
}

static void CopyArray(void ** A, size_t iBegin, size_t iEnd, void ** B)
{
    for(size_t k = iBegin; k < iEnd; k++){
        B[k] = A[k];
    }
}
