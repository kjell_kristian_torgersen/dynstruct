
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "rng.h"
#include "parray.h"
#include "collection.h"

extern void parray_mergesort_priv(void ** A, void ** B, size_t n,int (*compare_fn)(const void*,const void*));
extern void parray_quicksort_priv(void ** A, size_t lo, size_t hi, int(*compare_fn)(const void*,const void*));
extern iter_t * parray_iter_init(void ** first, void ** last);

struct parray {
        col_vtable_t * vtable;          ///< vtable to conform to the collection interface
        void ** data;                   ///< the array of pointers
        size_t size;                    ///< size of array in number of pointers
        size_t capacity;                ///< current capacity in number of pointers
        void (*free_fn)(void *ptr);     ///< function to delete/free pointers with or NULL if you don't want to delete them when they go out of scope
};

static void changeCapacity(parray_t * this, size_t newCapacity) 
{
        void ** newData = malloc(newCapacity*sizeof(void*));

	if(newCapacity > this->size) {
		// New capacity is bigger than current size
        	memcpy(newData, this->data, this->size*sizeof(void*));
	} else {
		// New capacity is smaller than current size, only copy what we have room for and update this->size accordingly
		memcpy(newData, this->data, newCapacity*sizeof(void*));

                // Erase underlying memory for excessive pointers
                if(this->free_fn) {
                        for(size_t i = newCapacity; i < this->size; i++) {
                                this->free_fn(this->data[i]);
                        }
                }
                // Update size
		this->size = newCapacity;
	}

        free(this->data);
        this->data=newData;
        this->capacity=newCapacity;
}

void parray_append(parray_t * this, void * p) 
{
	if(this) {
		if(this->size >= this->capacity) {
			changeCapacity(this, this->capacity*2);
		}
		this->data[this->size] = p;
		this->size++;

		//printf("%lu %lu\n", this->size, this->capacity);
	}
}

void parray_appendn(parray_t * this, void ** p, size_t count) 
{
        for(size_t i = 0; i < count; i++) {
                if(this->size >= this->capacity) {
                        changeCapacity(this, this->capacity*2);
                }
                this->data[this->size] = p[i];
        	this->size++;
        }
       // printf("%lu %lu\n", this->size, this->capacity);
}

ssize_t parray_indexOf(const parray_t * this, const void * p) 
{
        for(size_t i = 0; i < this->size; i++) {
                if(this->data[i] == p) {
                        return (ssize_t)i;
                }
        }
        return -1;
}

void ** parray_data(parray_t * this) 
{
        if(this) {
                return this->data;
        } else {
                return NULL;
        }
}

void * parray_at(parray_t * this, size_t idx) 
{
        if(this) {
                if(idx < this->size) {
                        return this->data[idx];
                }
        }
        return NULL;
}

bool parray_remove(parray_t * this, void * p) 
{
        ssize_t idx = parray_indexOf(this, p);
        if(idx != -1) {
                return parray_removeAt(this, idx);
        } else {
                return false;
        }
}

bool parray_removeAt(parray_t * this, size_t idx) 
{
        if(idx < this->size) {
                if(this->free_fn) {
                        this->free_fn(this->data[idx]);
                }
                for(size_t i = idx; i < this->size - 1; i++) {
                        this->data[i] = this->data[i+1];
                }
                this->size--;
                return true;
        } else {
                return false;
        }
}

size_t parray_size(const parray_t * this) 
{
	return this->size;
}

size_t parray_capacity(const parray_t * this) 
{
	return this->capacity;
}

void parray_setCapacity(parray_t * this, size_t newCapacity) 
{
	changeCapacity(this, newCapacity);
}

void parray_fold(parray_t * this, void(*fold_fn)(void*elem,void*arg), void * arg)
{
        for(size_t i = 0; i < this->size; i++) {
                fold_fn(this->data[i], arg);
        }
}

void parray_foreach(parray_t * this, void(*foreach_fn)(void*elem))
{
        for(size_t i = 0; i < this->size; i++) {
                foreach_fn(this->data[i]);
        }
}

void parray_removeIf(parray_t * this, remove_if_fn remove)
{
        for(size_t i = 0; i < this->size; i++) {
                if(remove(this->data[i])) {
                        parray_removeAt(this, i);
                }
        }
}

void parray_filter(parray_t * this, bool(*filter_fn)(void*elem,void * arg), void * arg)
{
        for(size_t i = 0; i < this->size; i++) {
                if(filter_fn(this->data[i], arg)) {
                       parray_removeAt(this, i);
                }
        }
}

void parray_mergesort(parray_t * this, int (*compar)(const void *, const void *)) 
{
        void ** tmp = malloc(sizeof(void*)*this->size);
        parray_mergesort_priv(this->data, tmp, this->size, compar);
        // qsort(this->data, this->size, sizeof(void*), compar);
        free(tmp);
}

void parray_quicksort(parray_t * this, int(*compare_fn)(const void*,const void*)) {
        if(this) {
                if(this->size > 1) {
                        parray_quicksort_priv(this->data, 0, this->size-1, compare_fn);
                }
        }
}

void parray_shuffle(parray_t * this, rng_t * rng) 
{
        for (int i = this->size - 1; i > 0; i--) {  
                size_t j = rng_get(rng) % (i + 1);  
                void * tmp = this->data[i];
                this->data[i] = this->data[j];
                this->data[j] = tmp; 
        }  
}

void parray_clear(parray_t * this) 
{
        if(this) {
                if(this->free_fn) {
                        for(size_t i = 0; i < this->size; i++) {
                                this->free_fn(this->data[i]);
                        }
                }
                this->size = 0;
        }
}

void parray_free(parray_t * this) 
{
        if(this) {
                parray_clear(this);
                free(this->data);
                free(this);
        }
}

static int size(col_t *this) {   
        return parray_size((parray_t*)this);
}

static int elemsize(col_t *this) {
        (void)this;   
        return sizeof(void*);
}

static iter_t *get_iterator(col_t *this){
        parray_t * pa = (parray_t*)this;
        return parray_iter_init(pa->data, &pa->data[pa->size]);
}

static void clear_all(col_t *this) {
        parray_t * pa = (parray_t*)this;
        parray_clear(pa);
}

static int remove_if(col_t *this, remove_if_fn remove) {
        (void)this;
        (void)remove; // TODO: fix
    return -1;
}

static void efree(col_t * this) {
        parray_t * pa = (parray_t*)this;
        parray_free(pa);
}

static void * eadd(col_t * this, const void * data, int count) {
        (void)count;
        parray_t * pa = (parray_t*)this;
        parray_append(pa, (void*)data);
        return (void*) data;
}

static col_vtable_t vtable = {
    &eadd, &size, &elemsize, &get_iterator, &clear_all, &remove_if, &efree
};

parray_t * parray_init(void (*free_fn)(void *ptr)) 
{
        parray_t * this = malloc(sizeof(parray_t));
        if(this) {
                this->vtable = &vtable;
                this->size = 0;
                this->capacity = 10;
                this->data = malloc(sizeof(void*)*this->capacity);
                this->free_fn = free_fn;
        }
        return this;
}

void parray_push(parray_t *this, void * data)
{
        parray_append(this, data);
}

void parray_pop(parray_t *this)
{
        parray_removeAt(this, this->size-1);
}

void * parray_top(parray_t *this)
{
        return this->data[this->size - 1];
}