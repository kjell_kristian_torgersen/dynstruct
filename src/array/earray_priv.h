#ifndef __EARRAY_PRIV_H__
#define __EARRAY_PRIV_H__

#include "collection.h"
#include "iter.h"

typedef struct earray
{
    col_vtable_t *vtable;
    int elemsize; ///< size of one element in bytes
    int size;     ///< size of array in number of elements
    int capacity; ///< capacity of array
    char *data;   ///< data
} earray_t;

col_vtable_t * ea_vtable(void);

#endif