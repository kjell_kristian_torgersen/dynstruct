#include <stdlib.h>
#include <string.h>

#include "array.h"

typedef struct array {
    int size;
    char data[];
} array_t;

array_t * array_init(int size) 
{
    array_t * ret = malloc(size+sizeof(array_t));
    if(ret) {
        ret->size = size;
    }
    return ret;
}

int array_size(array_t * this) 
{
    return this->size;
}

void* array_data(array_t * this)
{
    return this->data;
}

void* array_at(array_t * this, int idx)
{
    return &this->data[idx];
}

void array_free(array_t * this) 
{
    if(this) {
        free(this);
    }
}

darray_t * array_to_darray(array_t * this) 
{
    darray_t * ret = da_init_alloc(this->size);
    if(ret) {
        memcpy(da_data(ret), this->data, this->size);
    }
    return ret;
}

earray_t * array_to_earray(array_t * this, int elemsize) 
{
    earray_t * ret = ea_init(elemsize);
    if(ret) {
        ea_appendn(ret, this->data, this->size/elemsize);
    }
    return ret;
}
