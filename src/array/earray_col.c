#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "earray.h"
#include "earray_priv.h"

#include "collection.h"
#include "iter.h"

typedef struct ea_iter {
    it_vtable_t * vtable;
    int step;
    char * data;
    char * end;
} ea_iter_t;

static void eit_next(iter_t * this) {
    ea_iter_t * it = (ea_iter_t*)this;
    it->data += it->step;
}

static bool eit_has_next(iter_t * this)
{
    ea_iter_t * it = (ea_iter_t*)this;
    return it->data < it->end;
}

static void* eit_data(iter_t * this){
    ea_iter_t * it = (ea_iter_t*)this;
    return it->data;
}

static it_vtable_t it_vtable = {
    eit_next, eit_has_next, eit_data, NULL
};

ea_iter_t * iter_create(earray_t * ea) {
    ea_iter_t * it = malloc(sizeof(ea_iter_t));
    if(it) {
        it->vtable = &it_vtable;
        it->step = ea->elemsize;
        it->data = ea->data;
        it->end = &ea->data[ea->size*ea->elemsize];
    }
    return it;
}

static int size(col_t *this) {   
    return ea_size((earray_t*)this);
}

static int elemsize(col_t *this) {   
    return ea_elemsize((earray_t*)this);
}

static iter_t *get_iterator(col_t *this){
    return (iter_t *)iter_create((earray_t*)this);
}

static void clear_all(col_t *this){
    ea_eraseall((earray_t*)this);
}

static int remove_if(col_t *this, remove_if_fn remove){
    return ea_remove_if((earray_t*)this, remove);
}

static void efree(col_t * this) {
    earray_t * ea = (earray_t *)this;
    ea_free(ea);
}

static void * eadd(col_t * this, const void * data, int count) {
    (void)count;
    return ea_append((earray_t*)this, data);
}

static col_vtable_t vtable = {
    &eadd, &size, &elemsize, &get_iterator, &clear_all, &remove_if, &efree
};

col_vtable_t * ea_vtable(void) 
{
    return &vtable;
}