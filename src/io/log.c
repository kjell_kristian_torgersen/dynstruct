#include <stdarg.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#include <stdbool.h>

#include "log.h"

static bool show_info = true;
static bool show_warning = true;
static bool show_error = true;

static FILE *logfile = NULL;

static void log_print(const char *type, const char *format, va_list arg) {
	time_t t = time(NULL);

	struct tm *tm = localtime(&t);
	char s[64];
	strftime(s, sizeof(s), "%F %T", tm);
	fprintf(logfile, "%s: %s: ", s, type);
	vfprintf(logfile, format, arg);
	fflush(logfile);
}

void log_open(const char *logpath) {
	logfile = fopen(logpath, "ab");
	log_info("Opening log.\n");
}

void log_close(void) {
	if (logfile) {
		log_info("Closing log.\n");
		fclose(logfile);
		logfile = NULL;
	}
}

void log_show_info(bool enable) 
{
    show_info = enable;
}

void log_show_warning(bool enable) {
    show_warning = enable;
}

void log_show_error(bool enable) {
    show_error = enable;
}

void log_info(const char *format, ...) {
	va_list arg;

	if (show_info) {
		printf("Info: ");
		va_start(arg, format);
		vprintf(format, arg);
		va_end(arg);
		va_start(arg, format);
		log_print("Info", format, arg);
		va_end(arg);
	}
	va_end(arg);
}

void log_warning(const char *format, ...) {
	va_list arg;

	if (show_warning) {
		printf("Warning: ");
		va_start(arg, format);
		vprintf(format, arg);
		va_end(arg);
		va_start(arg, format);
		log_print("Warning", format, arg);
		va_end(arg);
	}
}

void log_error(const char *format, ...) {
	va_list arg;

	if (show_error) {
		printf("Error: ");
		va_start(arg, format);
		vprintf(format, arg);
		va_end(arg);
		va_start(arg, format);
		log_print("Error", format, arg);
		va_end(arg);
	}
}