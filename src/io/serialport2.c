#include <asm/termios.h>

int ioctl(int fd, unsigned long request, ...);

void serialport_set_baudrate(int fd, int baudrate)
{
    struct termios2 tio;

    ioctl(fd, TCGETS2, &tio);
    tio.c_cflag &= ~CBAUD;
    tio.c_cflag |= BOTHER;
    tio.c_ispeed = baudrate;
    tio.c_ospeed = baudrate;
    ioctl(fd, TCSETS2, &tio);
}