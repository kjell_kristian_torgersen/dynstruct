#include <stdio.h>
#include <stdarg.h>

#include "col_factory.h"
#include "log.h"

#include "array.h"

array_t * file_read_all_bytes(const char *path)
{
	FILE *f = fopen(path, "rb");
	if (f)
	{
		fseek(f, 0, SEEK_END);
		long int fileSize = ftell(f);

		array_t *arr = array_init(fileSize);
		fseek(f, 0, SEEK_SET);
		fread(array_data(arr), 1, fileSize, f);
		fclose(f);
		return arr;
	}
	else
	{
		log_error("Error opening file: ");
		log_error(path);
		return NULL;
	}
}

void file_write_all_bytes(const char *path, array_t *bytes)
{

	FILE *f = fopen(path, "wb");
	if (f)
	{
		fwrite(array_data(bytes), array_size(bytes), 1, f);
		fclose(f);
	}
	else
	{
		log_error("Error opening file: ");
		log_error(path);
	}
}

col_t *file_read_all_lines(const char *path)
{
	col_t *lines = colfac_list();
	if (lines)
	{
		darray_t *ar = da_init();
		if (ar)
		{
			FILE *f = fopen(path, "rb");
			if (f)
			{
				int c;
				int n = 0;
				while ((c = getc(f)) != EOF)
				{
					if (c != '\n')
					{
						da_add(ar, &c, 1);
						n++;
					}
					else
					{
						col_add(lines, da_data(ar), n);
						da_remove_all(ar);
						n = 0;
					}
				}
				fclose(f);
			}
			da_free(ar);
		}
	}
	return lines;
}

void file_append(const char * filepath, const char * format, ...) 
{
	va_list arg;
	FILE * f = fopen(filepath, "ab");
	if(f) {
		va_start(arg, format);
		vfprintf(f, format, arg);
		va_end(arg);
		fclose(f);
	}
}

void file_read(const char * path, void * buf, int count) 
{
	FILE * f = fopen(path, "rb");
	if(f) {
		fread(buf, 1, count, f);
		fclose(f);
	}
}

void file_write(const char * path, const void * buf, int count) 
{
	FILE * f = fopen(path, "wb");
	if(f) {
		fwrite(buf, 1, count, f);
		fclose(f);
	}
}