#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "serialport.h"

int serialport_open(char *device, int baudrate, struct termios *oldtio)
{
    struct termios newtio;

    int fd = open(device, O_RDWR | O_NOCTTY);
    if (fd < 0)
    {
        perror(device);
        return -1;
    }

    if (oldtio)
        tcgetattr(fd, oldtio); /* save current port settings */

    memset(&newtio, 0, sizeof(newtio));
    newtio.c_cflag = /*baud |*/ CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;

    /* set input mode (non-canonical, no echo,...) */
    newtio.c_lflag = 0;

    newtio.c_cc[VTIME] = 50; /* inter-character timer unused */
    newtio.c_cc[VMIN] = 1;   /* blocking read until 1 chars received */

    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &newtio);

    serialport_set_baudrate(fd, baudrate);

    return fd;
}



void Set_LineMode(int fd, int linemode)
{
    struct termios newtio;
    tcgetattr(fd, &newtio); /* save current port settings */
    if (linemode)
    {
        newtio.c_lflag |= ICANON;
    }
    else
    {
        newtio.c_lflag &= (~ICANON);
    }
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &newtio);
}

/*
int Select_Simplified(struct ReadDataCallback * callbacks, int n)
{
	int ret = 1;
	fd_set readfds;
	char buf[1024];
	int maxfd = -1;
	FD_ZERO(&readfds);
	for (int i = 0; i < n; i++) {
		FD_SET(callbacks[i].fd, &readfds);
		if (maxfd < callbacks[i].fd)
			maxfd = callbacks[i].fd;
	}
	select(maxfd + 1, &readfds, NULL, NULL, NULL);
	for (int i = 0; i < n; i++) {
		if (FD_ISSET(callbacks[i].fd, &readfds)) {
			callbacks[i].callback(callbacks[i].fd, callbacks[i].arg);
		}
	}
	return ret;
}*/

/*
static void sig_pipe(int signo)
{
	Warning("SIGPIPE caught");
}

int Pipe_Create(char *cmd, int fds[2])
{
	int fd1[2], fd2[2];
	pid_t pid;

	if (signal(SIGPIPE, sig_pipe) == SIG_ERR)
		Warning("signal error");
	if (pipe(fd1) < 0 || pipe(fd2) < 0)
		Warning("pipe error");

	pid = fork();

	if (pid < 0) {
		Warning("fork error");
	} else if (pid == 0) {
		// child 
		close(fd1[1]);
		close(fd2[0]);
		if (fd1[0] != STDIN_FILENO) {
			if (dup2(fd1[0], STDIN_FILENO) != STDIN_FILENO) {
				Warning("dup2 error to stdin");
				return -1;
			}
			close(fd1[0]);
		}
		if (fd2[1] != STDOUT_FILENO) {
			if (dup2(fd2[1], STDOUT_FILENO) != STDOUT_FILENO) {
				Warning("dup2 error to stdout");
			}
			close(fd2[1]);
		}
		if (execl(cmd, "", (char *) 0) < 0) {
			Warning("execl error");
			return -1;
		}

	} else {
		close(fd1[0]);
		close(fd2[1]);
		fds[0] = fd2[0];
		fds[1] = fd1[1];
	}
	return 0;
}*/