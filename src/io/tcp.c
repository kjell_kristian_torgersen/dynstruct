#include <arpa/inet.h>
#include <errno.h>
#include <execinfo.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#include "log.h"

static void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int tcp_server(uint16_t port)
{
	int server_socket;
	struct sockaddr_in server_settings;
	int yes = 1;

	memset(&server_settings, 0, sizeof(server_settings));
	server_settings.sin_family = AF_INET;
	server_settings.sin_addr.s_addr = INADDR_ANY;
	server_settings.sin_port = htons(port);

	server_socket = socket(PF_INET, SOCK_STREAM, 0);

	// lose the pesky "Address already in use" error message
	if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
		log_error("setsockopt");
		return -1;
	}

	if (bind(server_socket, (struct sockaddr*) &server_settings, sizeof(server_settings)) != 0) {
		log_error("bind");
		return -1;
	}

	if (listen(server_socket, 5) != 0) {
		log_error("listen");
		return -1;
	}

	return server_socket;
}

int tcp_client(char * const server, int port)
{
	int sockfd;

	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];
	char cport[10];

	snprintf(cport, sizeof(cport), "%i", port);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(server, cport, &hints, &servinfo)) != 0) {
		log_warning("getaddrinfo: ");
		log_warning(gai_strerror(rv));
		return -1;
	}

	// loop through all the results and connect to the first we can
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			log_warning("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			log_warning("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL) {
		log_warning("client: failed to connect");
		return -1;
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *) p->ai_addr), s, sizeof s);
	log_info("client: connecting to: ");
	log_info(s);
	freeaddrinfo(servinfo); // all done with this structure
	return sockfd;
}