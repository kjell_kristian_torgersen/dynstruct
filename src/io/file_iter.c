#include <stdio.h>
#include <stdlib.h>

#include "iter.h"
#include "darray.h"

struct iter
{
	it_vtable_t *vtable;
	FILE *f;
	darray_t *line;
};

static void next(iter_t *this)
{
	int c;
	da_remove_all(this->line);
	while ((c = getc(this->f)) != '\n')
	{
		if (c == EOF)
			break;
		da_add(this->line, &c, 1);
	}
	c = '\0';
	da_add(this->line, &c, 1);
}

static bool has_next(iter_t *this)
{
	return !feof(this->f);
}

static void *data(iter_t *this)
{
	return da_data(this->line);
}

static void fi_free(iter_t *this)
{
	if (this->f)
	{
		fclose(this->f);
		this->f = NULL;
	}

	if (this->line)
	{
		da_free(this->line);
		this->line = NULL;
	}
	free(this);
}

static it_vtable_t vtable = {
    &next, &has_next, &data, &fi_free};

iter_t *file_iter_lines(const char *path)
{
	iter_t *this = malloc(sizeof(iter_t));
	if (this)
	{
		this->vtable = &vtable;
		this->f = fopen(path, "rb");
		this->line = da_init();
	}
	return this;
}