#include <stdio.h>
#include <string.h>

#include "datafile.h"
#include "file.h"
#include "iter.h"
#include "misc.h"
#include "dstring.h"

typedef struct parseinfo {
        datafile_t * datafile;
        char * description;
        int columns;
        datetime_t datetime;
        column_t * cols;
} parseinfo_t;

void parseLine(void * elem, void * arg) {
        char * line = elem;
        parseinfo_t * this = arg;
        printf("%s\n", line);
        if(line[0] == '#') {
                if(strfind(&line[2], "date: ")) {
                        datetime_fromstring(&this->datetime, &line[6]);
                } else if(strfind(&line[2], "description: ")) {
                        if(!this->description) {
                                this->description = strdup(&line[15]);
                        }
                }
        } else {
                col_t * words = string_split(line, ';');
                //col_foreach(words, (col_foreach_fn)puts);
                col_free(words);
        }
        
}

datafile_t * datafile_parse(char * path) 
{
        parseinfo_t parseInfo = {NULL, NULL, 0,{0},NULL};
        
        iter_t * lines = file_iter_lines(path);
        it_fold(lines, parseLine, &parseInfo);
        parseInfo.datafile = datafile_init(parseInfo.description, &parseInfo.datetime, parseInfo.columns, parseInfo.cols);
        return parseInfo.datafile;
}