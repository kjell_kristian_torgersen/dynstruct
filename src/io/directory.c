#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>


#include "directory.h"
#include "col_factory.h"

struct entry_info {
    char name[256];
    enum entry_type type;
};

col_t* dir_get_files(const char *path)
{

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(path)) != NULL)
    {
        //col_t *ret = colfac_earray(sizeof(struct entry_info));
        col_t *ret = colfac_list();
        /* print all the files and directories within directory */
        while ((ent = readdir(dir)) != NULL)
        {
            struct entry_info info;
            if(!strcmp(ent->d_name, ".")) continue;
            if(!strcmp(ent->d_name, "..")) continue;
            strncpy(info.name, ent->d_name, sizeof(info.name));
            if(ent->d_type & DT_DIR) {
                info.type = FT_DIR;
            } else {
                info.type = FT_FILE;
            }

            //printf("%s\n", ent->d_name);
            col_add(ret, &info, sizeof(info));
        }
        closedir(dir);
        return ret;
    }
    else
    {
        /* could not open directory */
        //fprintf(stderr, "Error enumerating %s\n", path);
        return NULL;
    }
}

char *dir_name(entry_info_t * this) 
{
    return this->name;
}

entry_type_t dir_type(entry_info_t * this) 
{
    return this->type;
}

long dir_size(char * filename)
{
    struct stat st;
    if(stat(filename, &st)==0) {
        return st.st_size;
    } else {
        return -1;
    }
}