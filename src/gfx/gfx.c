#include "gfx.h"

void gfx_open(gfx_t *this, int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t *callbacks, void * arg)
{
    this->vtable->open(this, width, height, pixelWidth, pixelHeight, callbacks, arg);
}
void gfx_clear(gfx_t *this)
{
    this->vtable->clear(this);
}
void gfx_drawrect(gfx_t *this, int x, int y, int w, int h, unsigned int color)
{
    this->vtable->drawrect(this, x, y, w, h, color);
}
void gfx_processEvents(gfx_t *this)
{
    this->vtable->processEvents(this);
}
void gfx_close(gfx_t *this)
{
    this->vtable->close(this);
}
