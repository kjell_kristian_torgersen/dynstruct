#include <GL/gl.h>
#include <GL/glx.h>
#include <X11/X.h>
#include <X11/Xlib.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "x11gfx.h"
#include "gfx.h"

struct x11gfx
{
    gfx_vtable_t *vtable;
    Display *display;
    int screen;
    Window window;
    GC gc;
    int width;
    int height;
    bool done;
    gfx_callbacks_t *callbacks;
    void *arg;
};

static void x11_open(gfx_t *this, int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t *callbacks, void *arg)
{
    (void)this;
    (void) width; (void) height; (void) pixelWidth; (void) pixelHeight; (void)callbacks; (void)arg;
}

static void x11_clear(gfx_t *this)
{
    x11gfx_clear((x11gfx_t *)this);
}

static void x11_drawrect(gfx_t *this, int x, int y, int w, int h, unsigned int color)
{
    x11gfx_drawrect((x11gfx_t *)this, x, y, w, h, color);
}
static void x11_processEvents(gfx_t *this)
{
    x11gfx_processEvents((x11gfx_t *)this);
}

static void x11_close(gfx_t *this)
{
    x11gfx_close((x11gfx_t *)this);
}

static gfx_vtable_t vtable = {&x11_open, &x11_clear, &x11_drawrect, &x11_processEvents, &x11_close};

x11gfx_t *x11gfx_open(int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t *callbacks, void *arg)
{
    x11gfx_t *this = malloc(sizeof(x11gfx_t));
    if (this)
    {
        this->vtable = &vtable;
        this->width = width;
        this->height = height;
        this->callbacks = callbacks;
        this->arg = arg;
        unsigned long black, white;

        this->display = XOpenDisplay((char *)0);
        this->screen = DefaultScreen(this->display);
        black = BlackPixel(this->display, this->screen),     /* get color black */
            white = WhitePixel(this->display, this->screen); /* get color white */

        this->window = XCreateSimpleWindow(this->display, DefaultRootWindow(this->display), 0, 0, width * pixelWidth, height * pixelHeight, 5, white, black);

        XSetStandardProperties(this->display, this->window, "My Window", "HI!", None, NULL, 0, NULL);

        // TODO: only enable events that have valid callback functions
        XSelectInput(this->display, this->window, ExposureMask | ButtonPressMask | PointerMotionMask | ButtonReleaseMask | KeyPressMask | KeyReleaseMask | StructureNotifyMask);

        this->gc = XCreateGC(this->display, this->window, 0, 0);

        XSetBackground(this->display, this->gc, white);
        XSetForeground(this->display, this->gc, black);

        XClearWindow(this->display, this->window);
        XMapRaised(this->display, this->window);

        int font = XLoadFont(this->display, "9x15bold");
        XSetFont(this->display, this->gc, font);
    }
    return this;
}

void x11gfx_processEvents(x11gfx_t *this)
{
    while (XPending(this->display))
    {
        XEvent event; /* the XEvent declaration !!! */

        XNextEvent(this->display, &event);
        switch (event.type)
        {
        case Expose:
            if (event.xexpose.count == 0)
            {
                /* the window was exposed redraw it! */
                //Redraw();
            }
            break;
        case KeyPress:
        {
            KeySym key;     /* a dealie-bob to handle KeyPress Events */
            char text[255]; /* a char buffer for KeyPress Events */
            /* get the next event and stuff it into our event variable.
				 Note:  only events we set the mask for are detected!
				 */
            //KeySym sym = XLookupKeysym(&event.xkey, 0);
            char c = 0;
            if (XLookupString(&event.xkey, text, 255, &key, 0) == 1)
            {
                printf("You pressed the %c key!\n", text[0]);
                c = text[0];
                this->callbacks->key_down(this->arg, event.xkey.x, event.xkey.y, c);
            }
        }
        //KeyDown(event);
        break;
        case KeyRelease:
        {
            KeySym key;     /* a dealie-bob to handle KeyPress Events */
            char text[255]; /* a char buffer for KeyPress Events */
            /* get the next event and stuff it into our event variable.
				 Note:  only events we set the mask for are detected!
				 */
            //KeySym sym = XLookupKeysym(&event.xkey, 0);
            char c = 0;
            if (XLookupString(&event.xkey, text, 255, &key, 0) == 1)
            {
                printf("You pressed the %c key!\n", text[0]);
                c = text[0];
                this->callbacks->key_up(this->arg, event.xkey.x, event.xkey.y, c);
            }
        }
        break;
        case ButtonPress:
            this->callbacks->mouse_down(this->arg, event.xbutton.x, event.xbutton.y, event.xbutton.button);
            break;
        case ButtonRelease:
            this->callbacks->mouse_up(this->arg, event.xbutton.x, event.xbutton.y, event.xbutton.button);
            break;
        case ConfigureNotify:
            //width = event.xconfigure.width / pixelWidth;
            //height = event.xconfigure.height / pixelHeight;
            //Redraw();
            break;
        case MotionNotify:
        {
            this->callbacks->mouse_motion(this->arg, event.xmotion.x, event.xmotion.y);
            //int mousex = event.xmotion.x / pixelWidth;
            //int mousey = event.xmotion.y / pixelHeight;
            //ctx->MouseMoved(mousex, mousey);
        }
        //Redraw();
        break;
        default:
            break;
        }
    }
}

void x11gfx_clear(x11gfx_t *this)
{
    XClearWindow(this->display, this->window);
}

void x11gfx_drawrect(x11gfx_t *this, int x, int y, int w, int h, unsigned int color)
{
    (void)color; // TODO: fix
    XSetForeground(this->display, this->gc, WhitePixel(this->display, 0));
    XDrawRectangle(this->display, this->window, this->gc, x, y, w, h);
}

void x11gfx_close(x11gfx_t *this)
{
    XFreeGC(this->display, this->gc);
    XDestroyWindow(this->display, this->window);
    XCloseDisplay(this->display);
    free(this);
}