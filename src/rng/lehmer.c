#include <stdlib.h>
#include <string.h>

#include "rng.h"

struct rng_state {
        rng_vtable_t * vtable;
        uint32_t state;
};

static void lseed(rng_t * this, uint32_t seed) 
{
        this->state = seed;
}

static uint32_t lget(rng_t * this) 
{
        this->state += 0xe120fc15;
        uint64_t a = (uint64_t)this->state * 0x4a39b70d;
        uint32_t b = (a >> 32) ^ a;
        a = (uint64_t)b * 0x12fad5c9;
        uint32_t c = (a >> 32) ^ a;
        return c;
}

static void lfree(rng_t * this) 
{
        if(this) {
                free(this);
        }
}

static void lwrite(rng_t *this, void *buf, int count)
{
	int n = count / 4;
	int rest = count & 3;
	uint32_t *data = buf;

	for (int i = 0; i < n; i++)
	{
		data[i] = lget(this);
	}
	if (rest)
	{
		uint32_t tmp = lget(this);
		memcpy(&data[n], &tmp, rest);
	}
}

static rng_vtable_t vtable = {&lseed, &lget, &lfree, &lwrite};

rng_t * lehmer_init(uint32_t seed) 
{
        rng_t * this = malloc(sizeof(rng_t));
        if(this) {
                this->vtable = &vtable;
                this->state = seed;
        }
        return this;
}
