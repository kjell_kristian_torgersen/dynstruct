#include <stdlib.h>
#include <string.h>

#include "rng.h"

#define A 0x9908B0DF
#define B 0x9D2C5680
#define C 0xEFC60000
#define F 1812433253
#define L 18
#define M 397
#define N 624
#define R 31
#define S 7
#define T 15
#define U 11

#define LOWER_MASK ((1ull << R) - 1)
#define UPPER_MASK (1ull << R)

typedef struct mt_state {
	rng_vtable_t *vtable;
	uint32_t mt[N];
	uint16_t index;
} mt_t;

static void lseed(rng_t *state, uint32_t seed) {
	mt_t *this = (mt_t *)state;
	uint32_t i;

	this->mt[0] = seed;

	for (i = 1; i < N; i++) {
		this->mt[i] = (F * (this->mt[i - 1] ^ (this->mt[i - 1] >> 30)) + i);
	}

	this->index = N;
}

static void generate(mt_t *this) {
	uint32_t i, x, y;

	for (i = 0; i < N; i++) {
		x = (this->mt[i] & UPPER_MASK) + (this->mt[(i + 1) % N] & LOWER_MASK);
		y = x >> 1;
		if (x & 0x1) y ^= A;
		this->mt[i] = this->mt[(i + M) % N] ^ y;
	}

	this->index = 0;
}

static uint32_t lget(rng_t *state) {
	mt_t *this = (mt_t *)state;
	uint32_t ret;

	if (this->index >= N) {
		generate(this);
	}

	ret = this->mt[this->index];
	this->index++;

	ret ^= (ret >> U);
	ret ^= (ret << S) & B;
	ret ^= (ret << T) & C;
	ret ^= (ret >> L);

	return ret;
}

static void lfree(rng_t *state) {
	if (state) {
		free(state);
	}
}

static void lwrite(rng_t *this, void *buf, int count)
{
	int n = count / 4;
	int rest = count & 3;
	uint32_t *data = buf;

	for (int i = 0; i < n; i++)
	{
		data[i] = lget(this);
	}
	if (rest)
	{
		uint32_t tmp = lget(this);
		memcpy(&data[n], &tmp, rest);
	}
}

static rng_vtable_t vtable = {&lseed, &lget, &lfree, &lwrite};

rng_t *mt_init(uint32_t seed) 
{
	mt_t *ret = malloc(sizeof(mt_t));
	if (ret) {
		ret->vtable = &vtable;
		lseed((rng_t *)ret, seed);
	}
	return (rng_t *)ret;
}
