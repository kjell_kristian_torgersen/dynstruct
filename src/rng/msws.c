#include <stdlib.h>
#include <string.h>

#include "rng.h"

struct rng_state
{
	rng_vtable_t *vtable;
	uint64_t x;
	uint64_t w;
	uint64_t s;
};

static void lseed(rng_t *this, uint32_t seed)
{
	(void)this;
	(void)seed;
}

static uint32_t lget(rng_t *this)
{
	this->x *= this->x;
	this->x += (this->w += this->s);
	return this->x = (this->x >> 32) | (this->x << 32);
}

static void lfree(rng_t *this)
{
	if (this)
	{
		free(this);
	}
}

static void lwrite(rng_t *this, void *buf, int count)
{
	int n = count / 4;
	int rest = count & 3;
	uint32_t *data = buf;

	for (int i = 0; i < n; i++)
	{
		data[i] = lget(this);
	}
	if (rest)
	{
		uint32_t tmp = lget(this);
		memcpy(&data[n], &tmp, rest);
	}
}

static rng_vtable_t vtable = {&lseed, &lget, &lfree, &lwrite};

rng_t *msws_init(void)
{
	rng_t *this = malloc(sizeof(rng_t));
	if (this)
	{
		this->vtable = &vtable;
		this->x = 0;
		this->w = 0;
		this->s = 0xb5ad4eceda1ce2a9;
	}
	return this;
}
