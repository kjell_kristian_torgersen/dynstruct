#include <stddef.h>
#include "rng.h"

static rng_t * _rng = NULL;

struct rng_state
{
        rng_vtable_t * vtable;
};

rng_t * grng(void) 
{
        if(_rng == NULL) {
                _rng = rngfile_init("/dev/random");
        }
        return _rng;
}

void rng_seed(rng_t * this, uint32_t seed) 
{
        this->vtable->seed(this, seed);
}

uint32_t rng_get(rng_t * this) 
{
        return this->vtable->get(this);
}

double rng_double(rng_t * this) 
{
        return (double)rng_get(this)/((double)UINT32_MAX);
}

int rng_range(rng_t * this, int min, int max) 
{
        double r = max - min;

        return (int)(rng_double(this) * r + min + 0.5);
}

void rng_free(rng_t * this) {
        this->vtable->free(this);
}

void rng_write(rng_t * this, void * buf, int count) 
{
	this->vtable->write(this, buf, count);
}

/** \brief Set global random number generator */
void rng_gset(rng_t * rng) 
{
        if(_rng) {
                rng_free(_rng);
        }
        _rng = rng;
}

/** \brief Seed global random number generator */
void rng_gseed(uint32_t seed) 
{
        if(_rng == NULL) {
                _rng = rngfile_init("/dev/random");
        } else {
                rng_seed(_rng, seed);
        }
}

/** \brief Get a random uint32_t from global random number generator */
uint32_t rng_u(void) 
{
        if(_rng == NULL) {
                _rng = rngfile_init("/dev/random");
        }
        return rng_get(_rng);
}

/** \brief Get a random double, 0 - 1 from global random number generator. */
double rng_d(void) 
{
        return (double)rng_u()/((double)UINT32_MAX);
}

void rng_gwrite(void * buf, int count)
{
	if(_rng == NULL) {
                _rng = rngfile_init("/dev/random");
        }
	rng_write(_rng, buf, count);
}