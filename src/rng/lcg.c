#include <stdlib.h>
#include <string.h>

#include "rng.h"

struct rng_state
{
	rng_vtable_t *vtable;
	uint32_t state;
	uint64_t m;
	uint64_t a;
	uint64_t c;
};

static void lseed(rng_t *this, uint32_t seed)
{
	this->state = seed;
}

static uint32_t lget(rng_t *this)
{
	this->state = (this->a * this->state + this->c) % this->m;
	return this->state;
}

static void lfree(rng_t *this)
{
	if (this)
	{
		free(this);
	}
}

static void lwrite(rng_t *this, void *buf, int count)
{
	int n = count / 4;
	int rest = count & 3;
	uint32_t *data = buf;

	for (int i = 0; i < n; i++)
	{
		this->state = (this->a * this->state + this->c) % this->m;
		data[i] = this->state;
	}
	if (rest)
	{
		this->state = (this->a * this->state + this->c) % this->m;
		uint32_t tmp = this->state;
		memcpy(&data[n], &tmp, rest);
	}
}

static rng_vtable_t vtable = {&lseed, &lget, &lfree, &lwrite};

rng_t *lcg_init(uint32_t seed, uint64_t m, uint64_t a, uint64_t c)
{
	rng_t *this = malloc(sizeof(rng_t));
	if (this)
	{
		this->vtable = &vtable;
		this->state = seed;
		this->m = m;
		this->a = a;
		this->c = c;
	}
	return this;
}
