#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rng.h"

struct rng_state
{
	rng_vtable_t *vtable;
	FILE * f;
};

static void lseed(rng_t *this, uint32_t seed)
{
	(void)this;
	(void)seed;
}

static uint32_t lget(rng_t *this)
{
	uint32_t r;
	fread(&r, sizeof(r), 1, this->f);
	return r;
}

static void lfree(rng_t *this)
{
	if (this)
	{
		if(this->f) {
			fclose(this->f);
		}
		free(this);
	}
}

static void lwrite(rng_t *this, void *buf, int count)
{
	fread(buf, count, 1, this->f);
}

static rng_vtable_t vtable = {&lseed, &lget, &lfree, &lwrite};

rng_t *rngfile_init(const char * path)
{
	rng_t *this = malloc(sizeof(rng_t));
	if (this)
	{
		this->vtable = &vtable;
		this->f = fopen(path, "rb");
	}
	return this;
}
