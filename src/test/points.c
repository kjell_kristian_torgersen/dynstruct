#include <stdio.h>
#include <stdlib.h>

#include "gfx.h"
#include "list.h"

static col_t * points = NULL;
static gfx_t * gfx = NULL;

typedef struct point
{
    int x;
    int y;
    int vy;
} point_t;

static void draw_point(void *elem)
{
    point_t *p = (point_t *)elem;
    gfx_drawrect(gfx, p->x, p->y >> 8, 2, 2, 0);
    p->y += p->vy;
    p->vy++;
}

static bool remove_point(void * elem) {
    point_t *p = (point_t *)elem;
    return (p->y>>8) > 600;
}

void points_init(gfx_t * _gfx) 
{
    points = (col_t*) list_init();
    gfx = _gfx;
}

void points_add(int x, int y) 
{
    point_t p = {x, y<<8,0};
    col_add(points, &p, sizeof(point_t));
    
}

int last_size = 0;

void points_update(void) 
{
        col_foreach(points, &draw_point);
        col_remove_if(points, &remove_point);
        int size = col_size(points);
        if(last_size != size) {
            printf("number of points = %i\n", size);
            last_size = size;
        }
}

void points_free(void) 
{
    col_free(points);
}
