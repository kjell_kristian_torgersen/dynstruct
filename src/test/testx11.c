#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "gfx.h"
#include "points.h"
#include "x11gfx.h"

#if 0
bool done = false;

static gfx_t * gfx;

void mouse_down(void *arg, int x, int y, int button)
{
   // printf("mouse_down: %i %i %i\n", x, y, button);
   // point_add(x,y);
}
void mouse_up(void *arg, int x, int y, int button)
{
    //printf("mouse_up: %i %i %i\n", x, y, button);
}
void key_down(void *arg, int x, int y, int key)
{
    //printf("key_down: %i %i %i\n", x, y, key);
    if (key == 'q')
        done = true;
}
void key_up(void *arg, int x, int y, int key)
{
    //printf("key_up: %i %i %i\n", x, y, key);
}
void mouse_motion(void *arg, int x, int y)
{
    //printf("%i %i\n", x, y);
     points_add(x,y);
}

gfx_callbacks_t callbacks = {&mouse_down, &mouse_up, &key_down, &key_up, &mouse_motion};

void test_x11(void)
{
    gfx = (gfx_t *)x11gfx_open(640, 480, 1, 1, &callbacks, NULL);
    points_init(gfx);
    while (!done)
    {
        gfx_processEvents(gfx);
        gfx_clear(gfx);
        points_update();
        

        usleep(10000);
    }

    gfx_close(gfx);
    points_free();
   
}
#endif