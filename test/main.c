#include <stdio.h>

#include "pqueue.h"
#include "earray.h"
#include "rng.h"

void pqueue_test(void) 
{
	pqueue_t * queue = pqueue_init();
	pqueue_enqueue(queue, "Hello ");
	pqueue_enqueue(queue, "World\n");
	while(pqueue_size(queue)) {
		printf("%s", pqueue_dequeue(queue));
	}
	pqueue_free(queue);
}

int intcmp(const void * a, const void *b) {
	const int * A = a;
	const int * B = b;
	return *A - *B;
}

void intprint(void * arg) 
{
	int * i = arg;
	printf("%i ", *i);
}

int main(int argc, char const *argv[])
{
	earray_t * ea = ea_init(sizeof(int));
	for(int i = 0; i < 100; i++) {
		int u = rng_range(grng(), 1, 6);
		ea_append(ea, &u);
	}
	ea_sort(ea, intcmp);
	ea_foreach(ea, intprint);
	
	ea_free(ea);
	return 0;
}
