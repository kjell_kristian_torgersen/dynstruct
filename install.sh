#!/bin/bash
mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
make
sudo rm /usr/include/dynstruct
sudo rm /usr/lib/libdynstruct.so
sudo ln -s /shared/programming/dynstruct/inc /usr/include/dynstruct
sudo ln -s /shared/programming/dynstruct/build/libdynstruct.so /usr/lib/libdynstruct.so
