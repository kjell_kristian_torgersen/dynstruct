#ifndef __POINT_H__
#define __POINT_H__

#include "gfx.h"

void points_init(gfx_t * _gfx);
void points_add(int x, int y);
void points_update(void);
void points_free(void);

#endif