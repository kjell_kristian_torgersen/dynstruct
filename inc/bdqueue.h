#ifndef INC_Bbdqueue
#define INC_Bbdqueue

#include <stddef.h>

typedef struct bdqueue bdqueue_t;

/** \brief Blocking data queue. (Should be thread safe.) */
bdqueue_t * bdqueue_init(void);
/** \brief Append an element of size specified by count bytes from address specified by *data */
void bdqueue_enqueue(bdqueue_t * this, const void * data, size_t count);
/** \brief Copy element from queue to buffer specified by buf, count bytes. MIN(available bytes, count) is returned and copied. Intentionally blocks until data is available. */
size_t bdqueue_dequeue(bdqueue_t * this, void * buf, size_t count);
/** \brief Size of queue in number of elements. */
size_t bdqueue_size(bdqueue_t * this);
/** \brief Free queue. Call bdqueue_done() before freeing queue and ensure all threads using this queue are joined before freeing up. */
void bdqueue_free(bdqueue_t * this);
/** \brief Returns all blocking bdqueue_dequeue() calls. */
void bdqueue_done(bdqueue_t * this);
#endif /* INC_Bbdqueue */
