#ifndef __COLUMN_H__
#define __COLUMN_H__
typedef struct column {
        char * name;
        char * unit;
} column_t;

void column_fromstring(column_t * this, char *str);
void column_free(column_t * this);
#endif // !__COLUMN_H__


