/** \file collection.h Interface for interacting with the various collections (arrays, linked lists...) the same way */
#ifndef __COLLECTION_H__
#define __COLLECTION_H__

typedef struct iter iter_t;

#include <stdbool.h>

//#include "iter.h"

typedef struct col col_t;
typedef void(*col_foreach_fn)(void *);
typedef void(*col_fold_fn)(void *, void *);
typedef bool(*remove_if_fn)(void*);

typedef struct col_vtable {
    void* (*add)(col_t * this, const void * data, int count);
    int (*size)(col_t * this);
    int (*elemsize)(col_t * this);
    iter_t* (*get_iterator)(col_t * this);
    void (*clear_all)(col_t * this);
    int (*remove_if)(col_t * this, remove_if_fn remove);
    void (*free)(col_t * this);
} col_vtable_t;

typedef struct col {
    col_vtable_t * vtable;
} col_t;

/** \brief Add an element to the collection
 * \param *this The collection
 * \param *data The data to add
 * \param count Number of bytes. Beware: some collections operate with a fixed size(arrays...) and are therefore ignoring this parameter
*/
void * col_add(col_t * this, const void * data, int count);
/** \brief Get the size of collection in number of elements */
int col_size(col_t * this);
/** \brief Remember to free the returned iterator after using */
iter_t * col_get_iterator(col_t * this);
/** \brief Delete all elements in collection */
void col_clear_all(col_t * this);
/** \brief Delete specified elements in collection */
void col_remove_if(col_t * this, remove_if_fn remove);
/** Free all memory used by collection*/
void col_free(col_t * this);
/** Call function specified with foreach_fn on each element in collection */
void col_foreach(col_t *this, col_foreach_fn foreach_fn);
/** Call function specified with fold_fn on each element in collection with an extra argument for storing state between function calls. (Ideal for things like finding min, max, average ... )*/
void col_fold(col_t *this, col_fold_fn fold_fn, void * arg);
#endif