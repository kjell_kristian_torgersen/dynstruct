/** \file iter.h Interface for using iterators on compatible collections */
#ifndef __ITER_H__
#define __ITER_H__

#include <stdbool.h>

#include "collection.h"

typedef struct iter iter_t;

typedef struct it_vtable {
    void (*next)(iter_t * this);
    bool (*has_next)(iter_t * this);
    void* (*data)(iter_t * this);
    void (*free)(iter_t * this);
} it_vtable_t;

iter_t *generator_iterator(void*(*func)(void *arg), void * arg);

void it_next(iter_t * this);
bool it_has_next(iter_t * this);
void* it_data(iter_t * this);
void it_free(iter_t * this);
void it_foreach(iter_t *it, col_foreach_fn foreach_fn);
void it_fold(iter_t *it, col_fold_fn fold_fn, void * arg);

#endif