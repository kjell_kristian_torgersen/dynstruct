#ifndef __DIRECTORY_H__
#define __DIRECTORY_H__

#include "collection.h"

typedef enum entry_type {FT_DIR, FT_FILE } entry_type_t;

typedef struct entry_info entry_info_t;

col_t* dir_get_files(const char *path);

char *dir_name(entry_info_t * this);
entry_type_t dir_type(entry_info_t * this);
long dir_size(char * filename);
#endif