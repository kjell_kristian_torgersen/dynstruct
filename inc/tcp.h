#ifndef __TCP_H__
#define __TCP_H__

#include <stdint.h>

int tcp_server(uint16_t port);
int tcp_client(char * const server, const char * port);

#endif