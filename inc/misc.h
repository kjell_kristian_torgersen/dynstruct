#ifndef __MISC_H__
#define __MISC_H__

#include <stdbool.h>

char *strmerge(const char *str, ...);
void * memdup(void * src, int count);
char * substr(char * str, int count);
bool strfind(char * str, char * what);

#endif