#ifndef INC_BPQUEUE
#define INC_BPQUEUE

#include <stddef.h>

typedef struct bpqueue bpqueue_t;

/** \brief Blocking pointer queue. (Should be thread safe.) */
bpqueue_t * bpqueue_init(void);
/** \brief Put the pointer specified with *data on the queue. */
void bpqueue_enqueue(bpqueue_t * this, void * data);
/** \brief Return a pointer from the queue. Intentionally block execution until pointer is available. */
void * bpqueue_dequeue(bpqueue_t * this);
/** \brief Size of queue in number of elements. */
size_t bpqueue_size(bpqueue_t * this);
/** \brief Free queue. Call bpqueue_done() before freeing queue and ensure all threads using this queue are joined before freeing up. */
void bpqueue_free(bpqueue_t * this);
/** \brief Returns all blocking bpqueue_dequeue() calls. */
void bpqueue_done(bpqueue_t * this);

#endif /* INC_BPQUEUE */
