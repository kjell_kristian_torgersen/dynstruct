#ifndef __DSTRING_H__
#define __DSTRING_H__

#include "collection.h"

col_t *string_split(const char *str, char sep);

#endif // !__DSTRING_H__