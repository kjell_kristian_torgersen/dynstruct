/** \file parray.h \brief Array of pointers implementation. Use cases are for games or programs that utilizes many similar "objects" that can be treated somewhat similarly. For example a forms library. */
#ifndef __PARRAY_H__
#define __PARRAY_H__

#include <stdint.h>
#include <stdbool.h>

#include "rng.h"

typedef struct parray parray_t;
/** \brief Create a pointer array. When a pointer is removed, the function specified with free_fn is called on that pointer to automatically free its memory. */
parray_t *parray_init(void (*free_fn)(void *ptr));
/** \brief Append pointer specified by *p at the end */
void parray_append(parray_t *this, void *p);
/** \brief Append count pointers specified by **p at the end */
void parray_appendn(parray_t *this, void **p, size_t count);
/** \brief Get the index of pointer specified by *p */
ssize_t parray_indexOf(const parray_t *this, const void *p);
/** \brief Get direct access to all stored pointers. */
void **parray_data(parray_t *this);
/** \brief Get access to pointer at index specified by idx.
 * \return pointer if idx is valid, NULL otherwise */
void *parray_at(parray_t *this, size_t idx);
/** \brief Remove an element specified by pointer *p. If free_fn is specified, it is used to remove underlying memory. */
bool parray_remove(parray_t *this, void *p);
/** \brief Remove an element specified by index idx. If free_fn is specified, it is used to remove underlying memory. */
bool parray_removeAt(parray_t *this, size_t idx);
/** \brief Get current size, number of pointers stored. */
size_t parray_size(const parray_t *this);
/** \brief Get current capacity before memory must be re-allocated, number of pointers. */
size_t parray_capacity(const parray_t *this);
/** \brief Change the capacity. */
void parray_setCapacity(parray_t *this, size_t newCapacity);
/** \brief Call fold_fn on each element, where access to the element and user specified argument *arg is provided. */
void parray_fold(parray_t *this, void (*fold_fn)(void *elem, void *arg), void *arg);
/** \brief Call foreach_fn on each element, where access to the element is provided. */
void parray_foreach(parray_t *this, void (*foreach_fn)(void *elem));
/** \brief Call filter_fn on each element, where access to the element and user specified arguement is provided. If this function returns true, the pointer is removed. */
void parray_filter(parray_t *this, _Bool (*filter_fn)(void *elem, void *arg), void *arg);
/** \brief Sort the array with mergesort. */
void parray_mergesort(parray_t *this, int (*compar)(const void *, const void *));
/** \brief Sort the array with quicksort. */
void parray_quicksort(parray_t *this, int (*compare_fn)(const void *, const void *));
/** \brief Randomize the elements in the array based on random number generator specified by *rng */
void parray_shuffle(parray_t * this, rng_t * rng);
/** \brief Delete all elements in array. If free_fn is specified, also delete underlying memory. */
void parray_clear(parray_t *this);
/** \brief Delete the array including all elements in array. If free_fn is specified, also delete underlying memory. */
void parray_free(parray_t *this);
/** \brief Stack interface: "Push" a pointer to the "stack", similar to append */
void parray_push(parray_t *this, void * data);
/** \brief Stack interface: "Pop" a pointer from the "stack" */
void parray_pop(parray_t *this);
/** \brief Stack interface: Access the top of the "stack" */
void * parray_top(parray_t *this);
#endif // __PARRAY_H__
