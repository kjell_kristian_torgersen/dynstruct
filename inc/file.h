#ifndef __FILE_H__
#define __FILE_H__

#include "array.h"

/** \brief Read all bytes from file specified by *path, returns data as array_t*. Remember to free the returned value after use. */
array_t * file_read_all_bytes(const char *path);
void file_write_all_bytes(const char *path, array_t * bytes);
col_t *file_read_all_lines(const char *path);
void file_append(const char * filepath, const char * format, ...);
iter_t *file_iter_lines(const char *path);
void file_read(const char * path, void * buf, int count);
void file_write(const char * path, const void * buf, int count);

#endif