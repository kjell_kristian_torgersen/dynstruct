#ifndef __SERIALPORT_H__
#define __SERIALPORT_H__

#include <termios.h>

int serialport_open(char *device, int baudrate, struct termios *oldtio);
void serialport_set_baudrate(int fd, int baudrate);

#endif