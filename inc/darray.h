/** \file darray.h Dynamic automatically expanding byte array */
#ifndef __DARRAY_H__
#define __DARRAY_H__

typedef struct darray darray_t;
/** \brief dynamically growing array implementation */
darray_t * da_init(void);
darray_t *da_init_alloc(int size);
void da_add(darray_t * this, const void * data, int count);
void da_insert(darray_t * this, int pos, const void * data, int count);
void da_remove(darray_t * this, int pos, int count);
void * da_at(darray_t * this, int pos);
void * da_data(darray_t * this);
void da_remove_all(darray_t * this);
void da_free(darray_t * this);
int da_size(darray_t * this);
char * da_toString(darray_t * this);
#endif