/** \file slist.h Linked list with fixed node size */
#ifndef __SLIST_H__
#define __SLIST_H__

typedef struct slist slist_t;
typedef struct node node_t;

slist_t * sl_init(int elemSize);
void * sl_append(slist_t * sl, const void * data);
int sl_size(slist_t * this);
int sl_elemsize(slist_t *this);
void sl_clear_all(slist_t *this);
node_t * sl_get_iterator(slist_t * sl);
node_t * sl_next(node_t * node);
void * sl_get(node_t * node);
void sl_free(slist_t * sl);

#endif