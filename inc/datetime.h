#ifndef __DATETIME_H__
#define __DATETIME_H__

#include <stdint.h>

typedef struct datetime {
        uint16_t year;
        uint8_t month;
        uint8_t day;
        uint8_t hour;
        uint8_t minute;
        double second;
} datetime_t;

void datetime_init(datetime_t * this, uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, double second);
void datetime_fromstring(datetime_t * this, char * str);

#endif // !__DATETIME_H__