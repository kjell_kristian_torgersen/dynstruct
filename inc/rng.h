#ifndef __RNG_H__
#define __RNG_H__

#include <stdint.h>

/// \brief Implementation dependent struct for storing the state of the random number generator imeplementation
typedef struct rng_state rng_t;

/// \brief vtable for random number generation implementations.
typedef struct rng_vtable {
        void (*seed)(rng_t * this, uint32_t seed);      ///< Seed the (pseudo) random number generator implementation
        uint32_t (*get)(rng_t * this);                  ///< Get a random 32-bit unsigned integer from the implementation
        void (*free)(rng_t * this);                     ///< Free resources used by the random number generator
	void (*write)(rng_t * this, void * buf, int count);///< Write count random bytes to to location specified by *buf
} rng_vtable_t;

/** \brief Access the global random number generator. A global random number generator is included for convenience and can be used from the rng_gseed(), rng_u(), rng_d() and rng_gwrite()-functions. Naturally not thread safe. */
rng_t * grng(void);

/** \brief Seed the specified rng */
void rng_seed(rng_t * this, uint32_t seed);

/** \brief Get a random unsigned 32 bit integer */
uint32_t rng_get(rng_t * this);

/** \brief Get a random double in range 0 - 1 */
double rng_double(rng_t * this);

/** \brief Get a random integer in range min - max, including min and max. */
int rng_range(rng_t * this, int min, int max);

/** \brief Free a random number generator */
void rng_free(rng_t * this);

/** \brief Write count bytes to address specified by buf */
void rng_write(rng_t * this, void * buf, int count);

/** \brief Lehmer based rng */
rng_t * lehmer_init(uint32_t seed);

/** \brief Linear congruential generator*/
rng_t * lcg_init(uint32_t seed, uint64_t m, uint64_t a, uint64_t c);

/** \brief Random data from file. Can use /dev/urandom or /dev/random if feasible as *path */
rng_t * rngfile_init(const char * path);

/** \brief Mersenne twister */
rng_t * mt_init(uint32_t seed);

/** \brief Middle Square Weyl Sequence */
rng_t *msws_init(void);

/** \brief Seed global random number generator */
void rng_gseed(uint32_t seed);

/** \brief Random unsigned 32 bit int from global random number generator */
uint32_t rng_u(void);

/** \brief Random double in range 0-1 from global random number generator */
double rng_d(void);

/** \brief Write count bytes to address specified by buf from global random number generator */
void rng_gwrite(void * buf, int count);

#endif // __RNG_H__
