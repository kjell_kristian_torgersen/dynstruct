#ifndef __DATAFILE_H__
#define __DATAFILE_H__

#include "datetime.h"
#include "collection.h"
#include "column.h"

typedef struct datafile datafile_t;

datafile_t * datafile_init(char * description, datetime_t * datetime, int columns, column_t * cols);
char * datafile_description(datafile_t * this);
datetime_t * datafile_datetime(datafile_t * this);
void datafile_addrow(datafile_t * this, double * row);
col_t * datafile_getrows(datafile_t * this);
void datafile_free(datafile_t * this);

#endif // !__DATAFILE_H__