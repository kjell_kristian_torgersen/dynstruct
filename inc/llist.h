#ifndef __LLIST_H__
#define __LLIST_H__

typedef struct llist llist_t;

llist_t * ll_init();
void ll_addfront(llist_t * this, void * data, int count);

#endif