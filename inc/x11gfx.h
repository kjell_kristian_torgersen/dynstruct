/** \file x11gfx.h X11 graphics implementation conforming to interface specified in gfx.h */
#ifndef __X11GFX_H__
#define __X11GFX_H__

#include <stdbool.h>

#include "gfx.h"

typedef struct x11gfx x11gfx_t;

typedef struct x11gfx_callbacks {
    void(*mouse_down)(int x, int y, int button);
    void(*mouse_up)(int x, int y, int button);
    void(*key_down)(int x, int y, int key);
    void(*key_up)(int x, int y, int key);
    void(*mouse_motion)(int x, int y);
} x11gfx_callbacks_t;

x11gfx_t *x11gfx_open(int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t * callbacks, void * arg);
void x11gfx_clear(x11gfx_t * this);
void x11gfx_drawrect(x11gfx_t *this, int x, int y, int w, int h, unsigned int color);
void x11gfx_processEvents(x11gfx_t *this);
void x11gfx_close(x11gfx_t * this);

#endif