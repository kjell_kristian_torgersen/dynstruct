#ifndef __COL_FACTORY_H__
#define __COL_FACTORY_H__

#include "collection.h"

col_t * colfac_earray(int elemsize);
col_t * colfac_list(void);

#endif