#ifndef INC_DQUEUE
#define INC_DQUEUE

#include <stddef.h>

typedef struct dqueue dqueue_t;

dqueue_t * dqueue_init(void);
void dqueue_enqueue(dqueue_t * this, const void * data, size_t count);
size_t dqueue_dequeue(dqueue_t * this, void * buf, size_t count);
size_t dqueue_size(dqueue_t * this);
void dqueue_free(dqueue_t * this);

#endif /* INC_DQUEUE */
