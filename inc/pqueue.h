#ifndef INC_PQUEUE
#define INC_PQUEUE

#include <stddef.h>

typedef struct pqueue pqueue_t;

/** \brief Initialize a queue (FIFO) of pointers */
pqueue_t * pqueue_init(void);
/** \brief Put pointer specified by *data on queue */
void pqueue_enqueue(pqueue_t * this, void * data);
/** \brief Get a pointer from queue */
void * pqueue_dequeue(pqueue_t * this);
/** \brief Get the size of queue in number of elements */
size_t pqueue_size(pqueue_t * this);
/** \brief Free the queue. */
void pqueue_free(pqueue_t * this);

#endif /* INC_PQUEUE */
