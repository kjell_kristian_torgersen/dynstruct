#ifndef __LOG_H__
#define __LOG_H__

#include <stdbool.h>

void log_open(const char * logfile);
void log_show_info(bool enable);
void log_show_warning(bool enable);
void log_show_error(bool enable);
void log_info(const char * fmt, ...);
void log_warning(const char * fmt, ...);
void log_error(const char * fmt, ...);
void log_close(void);

#endif