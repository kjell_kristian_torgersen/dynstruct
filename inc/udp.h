#ifndef __UDP_H__
#define __UDP_H__

int udp_server(char * port);
int udp_connect(char * server, char * port);

#endif