#ifndef __DATAFILE_PARSE_H__
#define __DATAFILE_PARSE_H__

#include "datafile.h"

datafile_t * datafile_parse(char * path);

#endif // !__DATAFILE_PARSE_H__