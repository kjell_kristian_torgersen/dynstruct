/** \file gfx.h Interface for doing basic graphics */
#ifndef __GFX_H__
#define __GFX_H__

typedef struct gfx gfx_t;

/// set pointer to NULL to avoid subscribe to event
typedef struct gfx_callbacks { 
    void(*mouse_down)(void * arg, int x, int y, int button);
    void(*mouse_up)(void * arg, int x, int y, int button);
    void(*key_down)(void * arg, int x, int y, int key);
    void(*key_up)(void * arg, int x, int y, int key);
    void(*mouse_motion)(void * arg, int x, int y);
} gfx_callbacks_t;

typedef struct gfx_vtable {
    void (*open)( gfx_t * this, int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t * callbacks, void * arg);
    void (*clear)(gfx_t * this);
    void (*drawrect)(gfx_t *this, int x, int y, int w, int h, unsigned int color);
    void (*processEvents)(gfx_t *this);
    void (*close)(gfx_t * this);
} gfx_vtable_t;

typedef struct gfx {
    gfx_vtable_t * vtable;
} gfx_t;


void gfx_open(gfx_t * this, int width, int height, int pixelWidth, int pixelHeight, gfx_callbacks_t * callbacks, void * arg);
void gfx_clear(gfx_t * this);
void gfx_drawrect(gfx_t *this, int x, int y, int w, int h, unsigned int color);
void gfx_processEvents(gfx_t *this);
void gfx_close(gfx_t * this);

#endif
