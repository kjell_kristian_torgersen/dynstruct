/** \file array.h Dynamically allocated static array */
#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "darray.h"
#include "earray.h"

typedef struct array array_t;

/** \brief Create a dynamically allocated static array but with size included. Remember to call free on returned value when done using it. */
array_t * array_init(int size);

/** \brief Get size of array in bytes */
int array_size(array_t * this);

/** \brief Get access to data in array */
void* array_data(array_t * this);

/** \brief Get access to data in array with index specified by idx */
void* array_at(array_t * this, int idx);

/** \brief Free memory used by array_t */
void array_free(array_t * this);

/** \brief Convert to dynamic byte array */
darray_t * array_to_darray(array_t * this);

/** \brief Convert to dynamic array with specified element size*/
earray_t * array_to_earray(array_t * this, int elemsize);

#endif