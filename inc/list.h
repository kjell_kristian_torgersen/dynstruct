/** \file list.h Linked list implementation that support different sized nodes*/
#ifndef __LIST_H__
#define __LIST_H__

#include "collection.h"

typedef struct node node_t;
typedef struct list list_t;

/** \brief Create a linked list that support nodes of different sizes */
list_t * list_init(void);
void * list_add(list_t * this, const void * data, int count);
void list_remove(list_t * this, node_t * it);
int list_remove_if(list_t *this, remove_if_fn remove);
void list_remove_all(list_t * this);
void list_foreach(list_t *this, col_foreach_fn foreach_fn);
void list_fold(list_t *this, col_fold_fn fold_fn, void * arg);
node_t * lit_get(const list_t * this);
node_t * lit_next(node_t * this);
void * lit_data(node_t * this);
int list_size(const list_t * this);
void list_free(list_t * this);
#endif