/** \file earray.h Dynamic automatically expanding array with specified element sizes*/
#ifndef __EARRAY_H__
#define __EARRAY_H__

#include <stdio.h>

#include "collection.h"

typedef struct earray earray_t;

typedef void(*ea_map_fn_t)(void * dest, const void * src);
typedef int (*ea_compare_fn_t)(const void *, const void *);
typedef void(*ea_foreach_fn_t)(void *elem);
typedef void(*ea_fold_fn_t)(void*elem,void*arg);

/** \brief Create a dynamically expanding array with elements of fixed size*/
earray_t * ea_init(int elemsize);

/** \brief Append one element at end of array 
 * \param *ea Pointer to array
 * \param *data Pointer to data
*/
void * ea_append(earray_t * ea, const void * data);
/** \brief Append multiple elements at end of array 
 * \param *ea Pointer to array
 * \param *data Pointer to data
 * \param count Number of elements to append
*/
void ea_appendn(earray_t * ea, const void * data, int count);

/** \brief Insert one element at index specified with idx from data. Moves elements to make room for new element */
void ea_insert(earray_t * ea, int idx, const void * data);

/** \brief Insert multiple element at index specified with idx from data. Moves elements to make room for new elements */
void ea_insertn(earray_t * ea, int idx, const void * data, int count);

/** \brief Get element at specified index idx */
void * ea_at(earray_t * ea, int idx);

/** \brief Get a pointer to the beginning of the array. \warning This pointer may become invalid when alterering the array*/
void * ea_data(earray_t * ea);

/** \brief Overwrite one element at index specified with idx */
void ea_write(earray_t * ea, int idx, const void * data);

/** \brief Overwrite multiple element at index specified with idx */
void ea_writen(earray_t * ea, int idx, const void * data, int count);

/** \brief Sort the array with compare function specified as compar*/
void ea_sort(earray_t * ea, ea_compare_fn_t compar);

/** \brief Apply map_fn to each element in ea and return new array */
earray_t * ea_map(earray_t *ea, int newElemSize, ea_map_fn_t map_fn);

/** \brief Call foreach_fn with each element as an argument */
void ea_foreach(earray_t *ea, ea_foreach_fn_t foreach_fn);

/** \brief Call fold_fn on each element but with a provided argument. (Similar to foreach, but state can be stored between function calls. Ideal for finding min, max, average... for collection)*/
void ea_fold(earray_t * ea, ea_fold_fn_t fold_fn, void * arg);

/** \brief Erase one element specified by index idx */
void ea_erase(earray_t * ea, int idx);

/** \brief Erase all elements */
void ea_eraseall(earray_t * ea);

/** \brief Erase specified elements (when remove returns true)*/
int ea_remove_if(earray_t * this, remove_if_fn remove);

/** \brief Get the current size in elements */
int ea_size(earray_t * ea);

/** \brief Get the size of an element in bytes */
int ea_elemsize(earray_t * ea);

/** \brief Set capacity of specified size */
void ea_set_capacity(earray_t *ea, int capacity);

/** \brief Get current capacity */
int ea_capacity(earray_t * ea);

/** \brief Free an earray */
void ea_free(earray_t * ea);

void ea_push(earray_t * ea, void * data);
void ea_pop(earray_t * ea);
void * ea_top(earray_t * ea);

void earray_toFile2(earray_t * this, FILE * f);
void earray_toFile(earray_t * this, const char * path);
earray_t * earray_fromFile2(FILE * f);
earray_t * earray_fromFile(const char * path);

#endif // __EARRAY_H__
